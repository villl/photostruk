﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Android.Content;
using Android.Content.Res;
using PhotoStruk.DeviceSpecificInterfaces;
using PhotoStruk.Droid.DeviceSpecificServices;
using PhotoStruk.Models;
using PhotoStruk.Services;
using Xamarin.Forms;
using Environment = System.Environment;

[assembly: Dependency(typeof(DeviceFile))]
namespace PhotoStruk.Droid.DeviceSpecificServices
{
    public class DeviceFile : IDeviceFile
    {
        public async Task<string> GetFileAsync(string fileName)
        {
            AssetManager assets = Android.App.Application.Context.Assets;
            using (StreamReader sr = new StreamReader(assets.Open("24.xml")))
            {
                return await sr.ReadToEndAsync();
            }
        }

        public string GetFilePath(string fileName)
        {
            // Android database file location
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, fileName);
        }

        public Task<List<Photograph>> GetPhotographs()
        {
            throw new NotImplementedException();
        }

        public async Task<List<Village>> GetVillages()
        {
            List<Village> villages = new List<Village>();
            AssetManager assets = Android.App.Application.Context.Assets;
            for (int i = 24; i < 57; i++)
            {
                using (StreamReader sr = new StreamReader(assets.Open($"villages/{i}.xml")))
                {
                    villages.Add(await XmlReader.GetVillage(sr.BaseStream));
                } 
            }
            return villages;
        }
        public async Task<List<Component>> GetComponents()
        {
            List<Component> components = new List<Component>();
            AssetManager assets = Android.App.Application.Context.Assets;
            for (int i = 12; i < 20; i++)
            {
                using (StreamReader sr = new StreamReader(assets.Open($"components/{i}.xml")))
                {
                    components.Add(await XmlReader.GetComponent(sr.BaseStream));
                }
            }
            return components;
        }
    }
}