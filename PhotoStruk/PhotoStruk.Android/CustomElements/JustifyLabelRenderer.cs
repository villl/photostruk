﻿using Android.Content;
using Android.OS;
using Android.Text;
using PhotoStruk.CustomElements;
using PhotoStruk.Droid.CustomElements;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(JustifyLabel), typeof(JustifyLabelRenderer))]
namespace PhotoStruk.Droid.CustomElements
{
    public class JustifyLabelRenderer : LabelRenderer
    {
        public JustifyLabelRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
                {
                    Control.JustificationMode = JustificationMode.InterWord;

                }
            }
        }
    }
}