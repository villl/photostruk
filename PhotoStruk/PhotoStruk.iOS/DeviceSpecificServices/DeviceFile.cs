﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using PhotoStruk.DeviceSpecificInterfaces;
using PhotoStruk.iOS.DeviceSpecificServices;
using PhotoStruk.Models;
using Xamarin.Forms;

[assembly: Dependency(typeof(DeviceFile))]
namespace PhotoStruk.iOS.DeviceSpecificServices
{
    public class DeviceFile : IDeviceFile
    {
        public Task<List<Component>> GetComponents()
        {
            throw new NotImplementedException();
        }

        public StreamReader GetFile(string fileName)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetFileAsync(string fileName)
        {
            throw new NotImplementedException();
        }

        public string GetFilePath(string fileName)
        {
            // iOS database file location
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, fileName);
        }

        public Task<List<Photograph>> GetPhotographs()
        {
            throw new NotImplementedException();
        }

        public Task<List<Village>> GetVillages()
        {
            throw new NotImplementedException();
        }
    }
}