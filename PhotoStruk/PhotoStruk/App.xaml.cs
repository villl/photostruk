﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PhotoStruk.Services;
using PhotoStruk.Views;
using PhotoStruk.Utils;
using Acr.UserDialogs;
using System.Threading;
using System.Collections.Generic;
using PhotoStruk.Models;
using Realms;
using System.Threading.Tasks;
using Xamarin.Essentials;
using PhotoStruk.Resources;

namespace PhotoStruk
{
    public partial class App : Application
    {
        public static InMemory InMemory { get; private set; }
        private CancellationTokenSource WebCancellationTokenSource => new CancellationTokenSource();
        private WebService WebService = new WebService(UserDialogs.Instance);
        public App()
        {
            Device.SetFlags(new[] {
                "StateTriggers_Experimental",
                "IndicatorView_Experimental",
                "CarouselView_Experimental",
                "MediaElement_Experimental",
                "Expander_Experimental",
                "CollectionView_Experimental",
                "SwipeView_Experimental"
            });

            InitializeComponent();
            InitializeDefaultValues();
            //InitializeDatabase();

            MainPage = new AppShell();
        }

        private void InitializeDefaultValues()
        {
            InMemory = new InMemory();
        }

        private async void InitializeDatabase()
        {
            if (!await PermissionHelper.CheckPermission<Permissions.StorageWrite>())
            {
                return;
            }

            // Synchronize local database one per day
            DateTime lastSync = Preferences.Get(Const.LastSync, DateTime.Now.AddDays(1).AddHours(1));
            if (lastSync > DateTime.Now.AddDays(1))
            {
                await LoadVillages();
                await LoadVillageImages();
                await LoadComponents();
                await LoadComponentImages(); 
                await LoadAreals();
                await LoadArealImages();
                Preferences.Set(Const.LastSync, DateTime.Now);
            }
        }

        private async Task LoadVillages()
        {
            List<Village> villages = await WebService.GetVillagesAsync(WebCancellationTokenSource.Token);
            if (villages != null)
            {
                SaveDataToDatabase(villages);
            }
        }

        private async Task LoadVillageImages()
        {
            List<VillageImage> villageImages = await WebService.GetVillageImagesAsync(WebCancellationTokenSource.Token);
            if (villageImages != null)
            {
                SaveDataToDatabase(villageImages);
            }
        }

        private async Task LoadComponents()
        {
            List<Component> components = await WebService.GetComponentsAsync(WebCancellationTokenSource.Token);
            if (components != null)
            {
                SaveDataToDatabase(components);
            }
        }

        private async Task LoadComponentImages()
        {
            List<ComponentImage> componentImages = await WebService.GetComponentImagesAsync(WebCancellationTokenSource.Token);
            if (componentImages != null)
            {
                SaveDataToDatabase(componentImages);
            }
        }

        private async Task LoadAreals()
        {
            List<Areal> areals = await WebService.GetArealsAsync(WebCancellationTokenSource.Token);
            if (areals != null)
            {
                SaveDataToDatabase(areals);
            }
        }

        private async Task LoadArealImages()
        {
            List<ArealImage> arealImages = await WebService.GetArealImagesAsync(WebCancellationTokenSource.Token);
            if (arealImages != null)
            {
                SaveDataToDatabase(arealImages);
            }
        }

        private void SaveDataToDatabase<TEntity>(List<TEntity> entities) where TEntity : RealmObject, new()
        {
            DbService<TEntity> dbService = new DbService<TEntity>();
            dbService.SaveItemsAsync(entities);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
