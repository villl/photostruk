﻿using PhotoStruk.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PhotoStruk.Services
{
    public static class XmlReader
    {
        const string HistoryCZ = "history_cz";
        const string HistoryDE = "history_de";
        const string DestructionCZ = "destruction_de";
        const string DestructionDE = "destruction_cz";
        const string Coordinates = "gpscoordinates";
        public static async Task<Village> GetVillage(Stream stream)
        {
            Village village = new Village();
            XDocument document = XDocument.Load(stream);

            village.Id = int.Parse(document.Root.Element(XName.Get("village_id")).Value);
            //village.CzechName = document.Root.Element(XName.Get("name_cz")).Value;
            //village.GermanName = document.Root.Element(XName.Get("name_de")).Value;
            //village.District = document.Root.Element(XName.Get("district")).Value;
            //village.AdministrativeMunicipality = document.Root.Element(XName.Get("administrative_municipality")).Value;
            //village.CatastralArea = document.Root.Element(XName.Get("cadastralArea")).Value;
            //XElement historyCZ = document.Root.Element(XName.Get(HistoryCZ));
            //if (historyCZ.HasElements)
            //{
            //    village.HistoryCZ = ReadHistory(historyCZ);
            //}
            //XElement historyDE = document.Root.Element(XName.Get(HistoryDE));
            //if (historyDE.HasElements)
            //{
            //    village.HistoryDE = ReadHistory(historyDE);
            //}
            XElement coordinates = document.Root.Element(XName.Get(Coordinates));
            GPSCoordinates gPS = ReadCoordinates(coordinates);
            village.Latitude = gPS.Latitude;
            village.Longitude = gPS.Longitude;

            return village;
        }

        static History ReadHistory(XElement element)
        {
            History history = new History();
            history.MiddleAges = element.Element(XName.Get("middle_ages")).Value;
            history.EarlyModernAges = element.Element(XName.Get("early_modern_ages")).Value;
            history.NineteenthCentury = element.Element(XName.Get("nineteenth_century")).Value;
            history.StateBefore1945 = element.Element(XName.Get("state_before_1945")).Value;
            history.StateAfter1945 = element.Element(XName.Get("state_after_1945")).Value;
            history.State1989 = element.Element(XName.Get("state_1989")).Value;
            history.Present = element.Element(XName.Get("present")).Value;
            return history;
        }

        static GPSCoordinates ReadCoordinates(XElement element)
        {
            GPSCoordinates coordinates = new GPSCoordinates();
            if (double.TryParse(element.Element(XName.Get("latitude")).Value, NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out double latResult))
            {
                coordinates.Latitude = latResult;
            }
            if (double.TryParse(element.Element(XName.Get("longitude")).Value, NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out double lngResult))
            {
                coordinates.Longitude = lngResult;
            }
            return coordinates;
        }

        static Destruction ReadDestruction(XElement element)
        {
            Destruction destruction = new Destruction();
            destruction.Degree = element.Element(XName.Get("degree")).Value;
            destruction.Type = element.Element(XName.Get("type")).Value;
            destruction.Demolition = element.Element(XName.Get("demolition")).Value;
            destruction.MaterialRecycling = element.Element(XName.Get("materialRecycling")).Value;
            destruction.DisposalOfResidues = element.Element(XName.Get("disposalOfResidues")).Value;
            destruction.SurfaceAdjustment = element.Element(XName.Get("surfaceAdjustment")).Value;
            destruction.TerrainRelicts = element.Element(XName.Get("terrainRelicts")).Value;
            return destruction;
        }

        public static async Task<Component> GetComponent(Stream stream)
        {
            Component component = new Component();
            XDocument document = XDocument.Load(stream);

            component.Id = int.Parse(document.Root.Element(XName.Get("component_id")).Value);
            //component.NameCzech = document.Root.Element(XName.Get("name_cz")).Value;
            //component.GermanName = document.Root.Element(XName.Get("name_de")).Value;
            //component.TypeCZ = document.Root.Element(XName.Get("type_cz")).Value;
            //component.TypeDE = document.Root.Element(XName.Get("type_de")).Value;
            component.Width = float.Parse(document.Root.Element(XName.Get("width")).Value, NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"));
            component.Height = float.Parse(document.Root.Element(XName.Get("height")).Value, NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"));
            //component.Depth = float.Parse(document.Root.Element(XName.Get("depth")).Value, NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"));
            XElement historyCZ = document.Root.Element(XName.Get(HistoryCZ));
            if (historyCZ.HasElements)
            {
                //component.History = ReadHistory(historyCZ);
            }
            XElement historyDE = document.Root.Element(XName.Get(HistoryDE));
            if (historyDE.HasElements)
            {
                //component.History = ReadHistory(historyDE);
            }
            //XElement coordinates = document.Root.Element(XName.Get(Coordinates));
            //GPSCoordinates gPS = ReadCoordinates(coordinates);
            //component.Latitude = gPS.Latitude;
            //component.Longitude = gPS.Longitude;
            //XElement destructionCZ = document.Root.Element(XName.Get(DestructionCZ));
            //if (destructionCZ.HasElements)
            //{
            //    component.DestructionCZ = ReadDestruction(destructionCZ);
            //}
            //XElement destructionDE = document.Root.Element(XName.Get(DestructionDE));
            //if (destructionDE.HasElements)
            //{
            //    component.DestructionDE = ReadDestruction(destructionDE);
            //}

            return component;
        }
    }

    internal class GPSCoordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
