﻿using ModernHttpClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PhotoStruk.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Xamarin.Essentials;

namespace PhotoStruk.Services
{

    /// <summary>
    /// Type of request to webserver
    /// </summary>
    public enum ERequestType
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    /// <summary>
    /// Type of response to webserver
    /// </summary>
    public enum EResponseType
    {
        JSON,
        XML
    }

    /// <summary>
    /// Test Connection ErrorCode
    /// </summary>
    public enum EErrorCode
    {
        Default = -1,
        Ok = 0,
        NoConnection,
        WrongHost,
        WrongPort
    }

    /// <summary>
    /// Request to web server encapsulation
    /// </summary>
    public class WebRequest
    {
        /// <summary>
        /// Send request and return response
        /// </summary>
        /// <typeparam name="TResultData">Return object</typeparam>
        /// <param name="url">Request url</param>
        /// <param name="headers">Dictionary of header parameters</param>
        /// <param name="requestType">ERequestType (GET, POST, etc...)</param>
        /// <param name="responseType">EResponseType (JSON, XML, etc...)</param>
        /// <param name="data">Object to send</param>
        /// <param name="token">Cancellation Token</param>
        /// <exception cref="WebUnauthorizedException">Response Status Code == 401 (Unauthorized)</exception>>
        /// <exception cref="WebInternalServerErrorException">Response Status Code == all others</exception>
        /// <exception cref="ResponseContentTypeException">Response Content-Type didn't match</exception>
        /// <returns></returns>
        public async Task<TResultData> SendAsync<TResultData>(string url, Dictionary<string, string> headers, ERequestType requestType, object data, CancellationToken token) where TResultData : class
        {
            using (var client = CreateHttpClient())
            {
                // Set Up Apiary Header key
                foreach (var item in headers)
                {
                    client.DefaultRequestHeaders.Add(item.Key, item.Value);
                }

                var response = await SendRequestAsync(url, requestType, data, client, token);

                // Parse response
                if (response != null && response.IsSuccessStatusCode)
                {
                    // Check headers
                    if (response.Content.Headers.ContentType.MediaType == "application/json")
                    {
                        var contentAsString = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<TResultData>(contentAsString);
                    }
                    if (response.Content.Headers.ContentType.MediaType == "image/jpeg" || response.Content.Headers.ContentType.MediaType == "image/png")
                    {
                        var bytes = await response.Content.ReadAsByteArrayAsync();
                         MemoryStream stream = new MemoryStream(bytes);
                        return stream as TResultData;
                    }
                    throw new ResponseContentTypeException($"{response.Content.Headers.ContentType.MediaType} content type is not allowed.");
                }
                else if (response != null && response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new WebUnauthorizedException(response.Content.ReadAsStringAsync().Result);
                }
                else if (response != null && response.StatusCode == HttpStatusCode.BadRequest)
                {
                    throw new WebBadRequestException(await response.Content.ReadAsStringAsync());
                }
                else if (response != null && response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    // hozeni chyby provezt cteni synchrone
                    throw new WebInternalServerException(response.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new WebInternalServerException();
                }
            }
        }

        public async Task<TResultData> SendImageAsync<TResultData>(string url, Dictionary<string, string> headers, MultipartFormDataContent form, CancellationToken token) where TResultData : class
        {
            using (var client = CreateHttpClient())
            {
                foreach (var item in headers)
                {
                    client.DefaultRequestHeaders.Add(item.Key, item.Value);
                }

                var response = await client.PostAsync(url, form, token);

                // Parse response
                if (response != null && response.IsSuccessStatusCode)
                {
                    // Check headers
                    if (response.Content.Headers.ContentType.MediaType == "application/json")
                    {
                        var contentAsString = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<TResultData>(contentAsString);
                    }
                    throw new ResponseContentTypeException($"{response.Content.Headers.ContentType.MediaType} content type is not allowed.");
                }
                else if (response != null && response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new WebUnauthorizedException(response.Content.ReadAsStringAsync().Result);
                }
                else if (response != null && response.StatusCode == HttpStatusCode.BadRequest)
                {
                    throw new WebBadRequestException(await response.Content.ReadAsStringAsync());
                }
                else if (response != null && response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new WebInternalServerException(response.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new WebInternalServerException(response.Content.ReadAsStringAsync().Result);
                }
            }
        }

        private static async Task<HttpResponseMessage> SendRequestAsync(string url, ERequestType requestType, object data, HttpClient client, CancellationToken token)
        {
            HttpResponseMessage response = null;

            switch (requestType)
            {
                case ERequestType.GET:
                    {
                        response = await client.GetAsync(url, token);
                        break;
                    }
                case ERequestType.POST:
                    {
                        HttpContent content;
                        switch (data)
                        {
                            case string text:
                                content = new StringContent(text, Encoding.UTF8);
                                break;
                            case JObject o:
                                content = new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");
                                break;
                            default:
                                content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8);
                                break;
                        }
                        response = await client.PostAsync(url, content, token);
                        break;
                    }
                case ERequestType.PUT:
                    {
                        HttpContent content;
                        switch (data)
                        {
                            case string text:
                                content = new StringContent(text, Encoding.UTF8);
                                break;
                            case JObject o:
                                content = new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");
                                break;
                            default:
                                content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8);
                                break;
                        }
                        response = await client.PutAsync(url, content, token);
                        break;
                    }
                case ERequestType.DELETE:
                    {
                        response = await client.DeleteAsync(url, token);
                        break;
                    }
                default:
                    break;
            }

            return response;
        }

        private byte[] ImageFileToByteArray(string path)
        {
            FileStream fs = File.OpenRead(path);
            byte[] bytes = new byte[fs.Length];
            fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
            fs.Close();
            return bytes;
        }

        private HttpClient CreateHttpClient()
        {
            return new HttpClient(new NativeMessageHandler
            {

                Timeout = TimeSpan.FromSeconds(30),
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip,
                ClientCertificateOptions = ClientCertificateOption.Automatic
            });
        }
    }
}

