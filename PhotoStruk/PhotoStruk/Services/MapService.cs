﻿using BruTile.MbTiles;
using Mapsui;
using Mapsui.Geometries;
using Mapsui.Layers;
using Mapsui.Projection;
using Mapsui.UI.Forms;
using Mapsui.Utilities;
using Mapsui.Widgets.ScaleBar;
using PhotoStruk.CustomElements;
using SQLite;
using System;
using System.Diagnostics;
using System.IO;

namespace PhotoStruk.Services
{
    public class MapService
    {
        readonly Map Map;
        public CustomMapView MapView { get; set; }
        public ILayer Ilayer { get; set; }

        public static string MbTilesLocation { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        public MapService()
        {
            Map = new Map
            {
                CRS = "EPSG:3857",
                Transformation = new MinimalTransformation()
            };

            var startPoint = SphericalMercator.FromLonLat(14.7936, 48.7636);
            Map.Home = n => n.NavigateTo(startPoint, Map.Resolutions[7]);

            if (Xamarin.Essentials.Connectivity.NetworkAccess != Xamarin.Essentials.NetworkAccess.Internet)
            {
                var layer = CreateMbTilesLayer(Path.Combine(MbTilesLocation, "kk.mbtiles"));
                Ilayer = layer ?? OpenStreetMap.CreateTileLayer();
            }
            else 
            {
                Ilayer = OpenStreetMap.CreateTileLayer();
            }
            //map.Layers.Add(OpenStreetMap.CreateTileLayer());
            //map.Layers.Add(CreatePointLayer());
            Map.Widgets.Add(new ScaleBarWidget(Map) { TextAlignment = Mapsui.Widgets.Alignment.Center, HorizontalAlignment = Mapsui.Widgets.HorizontalAlignment.Center, VerticalAlignment = Mapsui.Widgets.VerticalAlignment.Top });
            //Map.Widgets.Add(new Mapsui.Widgets.Zoom.ZoomInOutWidget { MarginX = 20, MarginY = 40 });

        }

        public Map GetMap()
        {
            Map.Layers.Add(Ilayer);
            return Map;
        }
        public void AddLayer(ILayer layer)
        {
            Map.Layers.Add(layer);
        }

        public void ClearLayers()
        {
            Map.Layers.Clear();
        }

        /// <summary>
        ///  As OSM uses spherical mercator coordinates, It will transform the lon lat 
        ///  coordinates to spherical mercator and navigate to that coordinate
        /// </summary>
        /// <param name="position"></param>
        /// <param name="res"></param>
        public void NavigateFromPosition(Position position, int res)
        {
            MapView.Navigator.NavigateTo(SphericalMercator.FromLonLat(position.Longitude, position.Latitude), Map.Resolutions[res]);
        }
        public void NavigateFromPosition(Point point, int res)
        {
            try
            {
                MapView.Navigator.NavigateTo(
                    SphericalMercator.FromLonLat(point.X, point.Y), Map.Resolutions[res]);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }

        public void NavigateFromPosition(Position position)
        {
            MapView.Navigator.NavigateTo(
                SphericalMercator.FromLonLat(position.Longitude, position.Latitude), Map.Resolutions[10]);
        }

        public void NavigateFromPosition(Point point)
        {
            MapView.Navigator.NavigateTo(
                SphericalMercator.FromLonLat(point.X, point.Y), Map.Resolutions[10]);
        }

        public static TileLayer CreateMbTilesLayer(string path)
        {
            if (!File.Exists(path))
            {
                throw new Exception("File not found");
            }
            var mbTilesTileSource = new MbTilesTileSource(new SQLiteConnectionString(path, true));
            var mbTilesLayer = new TileLayer(mbTilesTileSource);
            return mbTilesLayer;
        }
    }
}
