﻿using Realms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PhotoStruk.Services
{
    public class DbService<TEntity> where TEntity : RealmObject, new()
    {
        private readonly Realm _database;
        public DbService()
        {
            _database = Realm.GetInstance();
        }

        /// <summary>
        /// Get whole data rows from table user restricted
        /// </summary>
        /// <returns></returns>
        public List<TEntity> GetItems()
        {
            return _database.All<TEntity>().ToList();
        }

        public bool Dispose()
        {
            try
            {
                _database.Dispose();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public Realm Freeze()
        {
            return _database.Freeze();
        }

        /// <summary>
        /// Get whole data rows from table ordered and user restricted
        /// </summary>
        /// <returns></returns>
        //public Task<List<TEntity>> GetItemsOrderedAsync<TKey>(Expression<Func<TEntity, TKey>> expression)
        //{
        //    return _database.Table<TEntity>().Where(x => x.ClientId == App.InMemory.ClientId).OrderBy(expression).ToListAsync();
        //}

        ///// <summary>
        ///// Get whole data rows from table ordered descending and user restricted
        ///// </summary>
        ///// <returns></returns>
        //public Task<List<TEntity>> GetItemsOrderedDescendingAsync<TKey>(Expression<Func<TEntity, TKey>> expression)
        //{
        //    return _database.Table<TEntity>().Where(x => x.ClientId == App.InMemory.ClientId).OrderByDescending(expression).ToListAsync();
        //}

        ///// <summary>
        ///// Get whole data rows from table ordered and user restricted
        ///// </summary>
        ///// <returns></returns>
        //public Task<List<TEntity>> GetItemsWhereAndOrderedAsync<TKey>(Expression<Func<TEntity, bool>> whereExpression, Expression<Func<TEntity, TKey>> orderExpression)
        //{
        //    return _database.Table<TEntity>().Where(x => x.ClientId == App.InMemory.ClientId).Where(whereExpression).OrderBy(orderExpression).ToListAsync();
        //}

        ///// <summary>
        ///// Get whole data rows from table ordered descending and user restricted
        ///// </summary>
        ///// <returns></returns>
        //public Task<List<TEntity>> GetItemsWhereOrderedDescendingAsync<TKey>(Expression<Func<TEntity, bool>> whereExpression, Expression<Func<TEntity, TKey>> orderExpression)
        //{
        //    return _database.Table<TEntity>().Where(x => x.ClientId == App.InMemory.ClientId).Where(whereExpression).OrderByDescending(orderExpression).ToListAsync();
        //}

        ///// <summary>
        ///// Get data rows from table that pass condition user restricted
        ///// </summary>
        ///// <param name="expression">Condition</param>
        ///// <returns></returns>
        public List<TEntity> GetItemsWhere(Expression<Func<TEntity, bool>> expression)
        {
            return _database.All<TEntity>().Where(expression).ToList();
        }

        public List<TEntity> GetItemsFilter(string filter)
        {
            return _database.All<TEntity>().Filter(filter).ToList();
        }

        ///// <summary>
        ///// Get any from table that pass condition user restricted
        ///// </summary>
        ///// <param name="expression">Condition</param>
        ///// <returns></returns>
        //public async Task<bool> GetItemsAnyAsync(Expression<Func<TEntity, bool>> expression)
        //{
        //    return (await _database.Table<TEntity>().Where(x => x.ClientId == App.InMemory.ClientId).CountAsync(expression)) > 0;
        //}

        /// <summary>
        /// Get one data row user restricted
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TEntity GetById(long id)
        {
            return _database.Find<TEntity>(id);
        }

        /// <summary>
        /// Update or Create row to data table
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TEntity SaveItemAsync(TEntity entity)
        {
            var result = new TEntity();
            _database.Write(() =>
            {
                result = _database.Add(entity, true);
            });
            return result;
        }

        public void SaveItemsAsync(List<TEntity> entities)
        {
            _database.Write(() =>
            {
                foreach (var item in entities)
                {
                    _database.Add(item, true);
                }
            });
        }
        ///// <summary>
        ///// Update or Create row to data table
        ///// </summary>
        ///// <param name="entity"></param>
        ///// <returns></returns>
        //public Task<int> SaveItemByWebIdAsync(TEntity entity)
        //{
        //    entity.ClientId = App.InMemory.ClientId;

        //    var dbEntity = GetByWebId(entity.WebId).Result;
        //    if (dbEntity != null)
        //    {
        //        entity.Id = dbEntity.Id;
        //        return _database.UpdateAsync(entity);
        //    }
        //    else
        //    {
        //        return _database.InsertAsync(entity);
        //    }
        //}
        ///// <summary>
        ///// Update or Create row to data table
        ///// </summary>
        ///// <param name="entity"></param>
        ///// <param name="expression">Condition</param>
        ///// <returns></returns>
        //public Task<int> SaveItemByExpressionAsync(TEntity entity, Expression<Func<TEntity, bool>> expression)
        //{
        //    entity.ClientId = App.InMemory.ClientId;

        //    var dbEntity = _database.Table<TEntity>().Where(expression).FirstOrDefaultAsync().Result;
        //    if (dbEntity != null)
        //    {
        //        entity.Id = dbEntity.Id;
        //        return _database.UpdateAsync(entity);
        //    }
        //    else
        //    {
        //        return _database.InsertAsync(entity);
        //    }
        //}
        ///// <summary>
        ///// Delete row in data table
        ///// </summary>
        ///// <param name="entity"></param>
        ///// <returns></returns>
        //public Task<int> DeleteItemAsync(TEntity entity)
        //{
        //    return _database.DeleteAsync(entity);
        //}
        ///// <summary>
        ///// Delete row by id
        ///// </summary>
        ///// <param name="entity"></param>
        ///// <returns></returns>
        //public Task<int> DeleteItemAsync(long id)
        //{
        //    var entity = GetById(id);
        //    return _database.DeleteAsync(entity);
        //}
    }
}

