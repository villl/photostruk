﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Services.Exceptions
{
    [Serializable]
    public class WebBadRequestException : Exception
    {
        public WebBadRequestException() { }
        public WebBadRequestException(string message) : base(message) { }
        public WebBadRequestException(string message, Exception inner) : base(message, inner) { }
        protected WebBadRequestException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
