﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Services.Exceptions
{
    [Serializable]
    public class WebUnavailableException : Exception
    {
        public WebUnavailableException() { }
        public WebUnavailableException(string message) : base(message) { }
        public WebUnavailableException(string message, Exception inner) : base(message, inner) { }
        protected WebUnavailableException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
