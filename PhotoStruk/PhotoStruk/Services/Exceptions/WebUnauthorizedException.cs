﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Services.Exceptions
{
    [Serializable]
    public class WebUnauthorizedException : Exception
    {
        public WebUnauthorizedException() { }
        public WebUnauthorizedException(string message) : base(message) { }
        public WebUnauthorizedException(string message, Exception inner) : base(message, inner) { }
        protected WebUnauthorizedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
