﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Services.Exceptions
{
    [Serializable]
    public class ResponseContentTypeException : Exception
    {
        public ResponseContentTypeException() { }
        public ResponseContentTypeException(string message) : base(message) { }
        public ResponseContentTypeException(string message, Exception inner) : base(message, inner) { }
        protected ResponseContentTypeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
