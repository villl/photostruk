﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Services.Exceptions
{
    [Serializable]
    public class WebInternalServerException : Exception
    {
        public WebInternalServerException() { }
        public WebInternalServerException(string message) : base(message) { }
        public WebInternalServerException(string message, Exception inner) : base(message, inner) { }
        protected WebInternalServerException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
