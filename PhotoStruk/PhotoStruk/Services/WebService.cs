﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PhotoStruk.Models;
using PhotoStruk.Resources;
using PhotoStruk.Services.Exceptions;
using PhotoStruk.Utils;
using PhotoStruk.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PhotoStruk.Services
{
    public class WebService
    {

        private static readonly WebRequest _request = new WebRequest();
        private IUserDialogs Dialogs { get; }

        private string AccessToken { get; set; } = Preferences.Get(Const.AccessToken, string.Empty);

        private const string ApiUrl = "https://photostruk.azurewebsites.net/api/";

        // Apiari.io webservice version
        public const string ApiVersion = "v1/";

        /// <summary>
        /// Gets the preferred language of device setting.
        /// </summary>
        /// <returns>The preferred language.</returns>
        private static string GetPreferredLanguage()
        {
            return System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName + "/";
        }

        /// <summary>
        /// Sets up header.
        /// </summary>
        /// <returns>The up header.</returns>
        private Dictionary<string, string> GetServiceHeader()
        {
            return new Dictionary<string, string>()
            {
                {"Api-Client", "mobile"}
            };
        }

        /// <summary>
        /// Sets up header with api key.
        /// </summary>
        /// <param name="apiKey"></param>
        /// <returns>The up header</returns>
        private Dictionary<string, string> GetServiceHeader(string accessToken)
        {
            return new Dictionary<string, string>()
            {
                {"Api-Client", "mobile"},
                {"Authorization", "Bearer " + accessToken}
            };
        }

        private string UrlService = "";

        /// <summary>
        /// Webservice constructor
        /// </summary>
        /// <param name="baseViewModel">Base view model.</param>
        public WebService(IUserDialogs dialogs)
        {
            this.Dialogs = dialogs;
            // TODO: change prod/test
            IsProduction = false;
        }

        /// <summary>
        /// Change Release state for url services
        /// </summary>
        public bool IsProduction { get; set; } = false;

        /// <summary>
        /// Post json and recieve json
        /// </summary>
        /// <typeparam name="TResponse">Response Type</typeparam>
        /// <param name="serviceUrl">Service url without Host url</param>
        /// <param name="headers">Dictionary of header parameters</param>
        /// <param name="jObject">Object to send</param>
        /// <param name="token">Cancellation token</param>
        /// <returns></returns>
        private async Task<TResponse> PostAsync<TResponse>(string serviceUrl, Dictionary<string, string> headers, JObject jObject, CancellationToken token) where TResponse : class, new()
        {
            return await RequestWithExceptionHandlers<TResponse>(serviceUrl, headers, ERequestType.POST, jObject, token);
        }

        private async Task<TResponse> PostImageAsync<TResponse>(string serviceUrl, Dictionary<string, string> headers, MultipartFormDataContent form, CancellationToken token) where TResponse : class, new()
        {
            return await ImageRequestWithExceptionHandlers<TResponse>(serviceUrl, headers, form, token);
        }

        /// <summary>
        /// Get json and recieve json
        /// </summary>
        /// <typeparam name="TResponse">Response Type</typeparam>
        /// <param name="serviceUrl">Service url wihout Host url</param>
        /// <param name="headers">Dictionary of header parameters</param>
        /// <param name="jObject">Object to send</param>
        /// <param name="token">Cancellation token</param>
        /// <returns></returns>
        private async Task<TResponse> GetAsync<TResponse>(string serviceUrl, Dictionary<string, string> headers, JObject jObject, CancellationToken token) where TResponse : class, new()
        {
            return await RequestWithExceptionHandlers<TResponse>(serviceUrl, headers, ERequestType.GET, jObject, token);
        }

        private async Task<TResponse> PutAsync<TResponse>(string serviceUrl, Dictionary<string, string> headers, JObject jObject, CancellationToken token) where TResponse : class, new()
        {
            return await RequestWithExceptionHandlers<TResponse>(serviceUrl, headers, ERequestType.PUT, jObject, token);
        }

        private async Task<TResponse> DeleteAsync<TResponse>(string serviceUrl, Dictionary<string, string> headers, JObject jObject, CancellationToken token) where TResponse : class, new()
        {
            return await RequestWithExceptionHandlers<TResponse>(serviceUrl, headers, ERequestType.DELETE, jObject, token);
        }

        private async Task<TResponse> RequestWithExceptionHandlers<TResponse>(string serviceUrl, Dictionary<string, string> headers, ERequestType requestType, JObject jObject, CancellationToken token) where TResponse : class, new()
        {
            bool IsConfirmed = false;
            do
            {
                if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                {
                    IsConfirmed = false;
                }
                else if (Connectivity.NetworkAccess != NetworkAccess.Internet)
                {
                    IsConfirmed = await this.Dialogs.ConfirmAsync(R.CheckConnection, R.Offline, R.TryAgain, R.Close);
                    if (!IsConfirmed)
                    {
                        return null;
                    }
                }
            } while (IsConfirmed);

            try
            {
                return await _request.SendAsync<TResponse>(serviceUrl, headers, requestType, jObject, token);
            }
            catch (WebUnavailableException ex)
            {
                if (Enum.TryParse(ex.Message, out EErrorCode errorCode))
                {
                    string errorMessage = "";
                    switch (errorCode)
                    {
                        case EErrorCode.Default:
                            break;
                        case EErrorCode.NoConnection:
                            errorMessage = R.ErrorNoConnection;
                            break;
                        case EErrorCode.WrongHost:
                            errorMessage = R.ErrorWrongHost;
                            break;
                        case EErrorCode.WrongPort:
                            errorMessage = R.ErrorWrongPort;
                            break;
                        default:
                            break;
                    }
                    await this.Dialogs.AlertAsync(errorMessage, R.ErrorCommunication);
                }

                return null;
            }
            catch (Exception ex)
            {
                try
                {
                    if (ex.Message != null)
                    {
                        var errorModel = JsonConvert.DeserializeObject<ErrorModel>(ex.Message);
                        await this.Dialogs.AlertAsync(errorModel.Message, R.ErrorCommunication);
                    }
                    else
                    {
                        throw;
                    }
                }
                catch (Exception)
                {
                    if (IsProduction)
                    {
                        await this.Dialogs.AlertAsync(R.ErrorUnknown, R.ErrorWebService);
                    }
                    else
                    {
                        var errorMessageBuilder = new StringBuilder();
                        var exception = ex;

                        while (exception?.Message != null)
                        {
                            errorMessageBuilder.AppendLine(exception.Message);
                            exception = exception.InnerException;
                        }

                        await this.Dialogs.AlertAsync(errorMessageBuilder.ToString(), R.ErrorWebService);
                    }
                }

                return null;
            }
        }

        private async Task<TResponse> ImageRequestWithExceptionHandlers<TResponse>(string serviceUrl, Dictionary<string, string> headers, MultipartFormDataContent form, CancellationToken token) where TResponse : class, new()
        {
            bool IsConfirmed = false;
            do
            {
                if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                {
                    IsConfirmed = false;
                }
                else if (Connectivity.NetworkAccess != NetworkAccess.Internet)
                {
                    IsConfirmed = await this.Dialogs.ConfirmAsync(R.CheckConnection, R.Offline, R.TryAgain, R.Close);
                    if (!IsConfirmed)
                    {
                        return null;
                    }
                }
            } while (IsConfirmed);

            try
            {
                return await _request.SendImageAsync<TResponse>(serviceUrl, headers, form, token);
            }
            catch (WebUnavailableException ex)
            {
                if (Enum.TryParse(ex.Message, out EErrorCode errorCode))
                {
                    string errorMessage = "";
                    switch (errorCode)
                    {
                        case EErrorCode.Default:
                            break;
                        case EErrorCode.NoConnection:
                            errorMessage = R.ErrorNoConnection;
                            break;
                        case EErrorCode.WrongHost:
                            errorMessage = R.ErrorWrongHost;
                            break;
                        case EErrorCode.WrongPort:
                            errorMessage = R.ErrorWrongPort;
                            break;
                        default:
                            break;
                    }
                    await this.Dialogs.AlertAsync(errorMessage, R.ErrorCommunication);
                }

                return null;
            }
            catch (WebUnauthorizedException)
            {
                Dialogs.Toast("Je vyžadována autentizace");
                return null;
            }
            catch (WebBadRequestException ex)
            {
                await Dialogs.AlertAsync(ex.Message, R.ErrorCommunication);
                return null;
            }
            catch (Exception ex)
            {
                try
                {
                    if (ex.Message != null)
                    {
                        var errorModel = JsonConvert.DeserializeObject<ErrorModel>(ex.Message);
                        await this.Dialogs.AlertAsync(errorModel.Message, R.ErrorCommunication);
                    }
                    else
                    {
                        throw;
                    }
                }
                catch (Exception)
                {
                    if (IsProduction)
                    {
                        await this.Dialogs.AlertAsync(R.ErrorUnknown, R.ErrorWebService);
                    }
                    else
                    {
                        var errorMessageBuilder = new StringBuilder();
                        var exception = ex;

                        while (exception?.Message != null)
                        {
                            errorMessageBuilder.AppendLine(exception.Message);
                            exception = exception.InnerException;
                        }

                        await this.Dialogs.AlertAsync(errorMessageBuilder.ToString(), R.ErrorWebService);
                    }
                }

                return null;
            }
        }

        public async Task<LoginResponse> LoginAsync(string email, string password, CancellationToken cancellationToken)
        {
            JObject signInData = JObject.FromObject(new
            {
                email,
                password
            });
            return await PostAsync<LoginResponse>(ApiUrl + "auth/login", GetServiceHeader(), signInData, cancellationToken);
        }

        public async Task<LoginResponse> RegisterAsync(string email, string password, string firstName, string lastName, CancellationToken cancellationToken)
        {
            JObject signInData = JObject.FromObject(new
            {
                firstName,
                lastName,
                email,
                password
            });
            return await PostAsync<LoginResponse>(ApiUrl + "auth/register", GetServiceHeader(), signInData, cancellationToken);
        }

        public async Task<LoginResponse> ExternalLoginAsync(string email, string firstName, string lastName, CancellationToken cancellationToken)
        {
            JObject signInData = JObject.FromObject(new
            {
                email,
                firstName,
                lastName
            });
            return await PostAsync<LoginResponse>(ApiUrl + "auth/externallogin", GetServiceHeader(), signInData, cancellationToken);
        }

        public async Task<LoginResponse> ChangePasswordAsync(string oldPassword, string newPassword, CancellationToken cancellationToken)
        {
            JObject signInData = JObject.FromObject(new
            {
                oldPassword,
                newPassword
            });
            return await PostAsync<LoginResponse>(ApiUrl + "auth/changepassword", GetServiceHeader(AccessToken), signInData, cancellationToken);
        }

        public async Task<List<Village>> GetVillagesAsync(CancellationToken cancelToken)
        {
            return await GetAsync<List<Village>>(ApiUrl + "village/cz", GetServiceHeader(), null, cancelToken);
        }

        public async Task<List<VillageImage>> GetVillageImagesAsync(CancellationToken cancelToken)
        {
            return await GetAsync<List<VillageImage>>(ApiUrl + "village/images", GetServiceHeader(), null, cancelToken);
        }

        public async Task<List<Component>> GetComponentsAsync(CancellationToken cancelToken)
        {
            return await GetAsync<List<Component>>(ApiUrl + "component/cz", GetServiceHeader(), null, cancelToken);
        }

        public async Task<List<ComponentImage>> GetComponentImagesAsync(CancellationToken cancelToken)
        {
            return await GetAsync<List<ComponentImage>>(ApiUrl + "component/images", GetServiceHeader(), null, cancelToken);
        }

        public async Task<List<Areal>> GetArealsAsync(CancellationToken cancelToken)
        {
            return await GetAsync<List<Areal>>(ApiUrl + "areal/cz", GetServiceHeader(), null, cancelToken);
        }

        public async Task<List<ArealImage>> GetArealImagesAsync(CancellationToken cancelToken)
        {
            return await GetAsync<List<ArealImage>>(ApiUrl + "areal/image", GetServiceHeader(), null, cancelToken);
        }

        public async Task<Comment> AddCommentAsync(string text, long userId, long imageId, CancellationToken cancelToken)
        {
            JObject comment = JObject.FromObject(new
            {
                text,
                userId,
                imageId
            });
            return await PostAsync<Comment>(ApiUrl + "comment", GetServiceHeader(AccessToken), comment, cancelToken);
        }

        public async Task<Comment> EditCommentAsync(string text, long commentId, CancellationToken cancelToken)
        {
            JObject comment = JObject.FromObject(new
            {
                text
            });
            return await PutAsync<Comment>(ApiUrl + "comment/" + commentId, GetServiceHeader(AccessToken), comment, cancelToken);
        }

        public async Task<Comment> DeleteCommentAsync(long commentId, CancellationToken cancelToken)
        {
            return await DeleteAsync<Comment>(ApiUrl + "comment/" + commentId, GetServiceHeader(AccessToken), null, cancelToken);
        }

        public async Task<List<Comment>> GetCommentImagesAsync(long imageId, CancellationToken cancelToken)
        {
            return await GetAsync<List<Comment>>(ApiUrl + "comment/image/" + imageId, GetServiceHeader(AccessToken), null, cancelToken);
        }

        public async Task<StreamImageSource> GetImageAsync(long imageId, CancellationToken cancelToken)
        {
            var memoryStream = await GetAsync<MemoryStream>(ApiUrl + "image/" + imageId, GetServiceHeader(), null, cancelToken);
            CustomStreamImageSource streamSource = new CustomStreamImageSource
            {
                Key = imageId.ToString(),
                Stream = (cancelToken) => Task.FromResult<Stream>(memoryStream)
            };

            return streamSource;
        }

        public async Task<User> GetUserAsync(long userId, CancellationToken cancelToken)
        {
            return await GetAsync<User>(ApiUrl + "user/" + userId, GetServiceHeader(AccessToken), null, cancelToken);
        }

        public async Task<Comment> LikeAsync(bool isLiked, long userId, long commentId, CancellationToken cancelToken)
        {
            JObject like = JObject.FromObject(new
            {
                isLiked,
                userId,
                commentId
            });
            return await PostAsync<Comment>(ApiUrl + "like", GetServiceHeader(AccessToken), like, cancelToken);
        }

        public async Task<List<Photo>> GetPhotoImagesAsync(long objectId, CancellationToken cancelToken)
        {
            return await GetAsync<List<Photo>>(ApiUrl + "photo/object/" + objectId, GetServiceHeader(), null, cancelToken);
        }

        public async Task<Photo> AddPhotoAsync(string name, string description,
            double? latitude, double? longitude, long objectId, MediaFile photo, long userId, CancellationToken cancelToken)
        {
            HttpContent photoStreamContent = new StreamContent(photo.GetStream());
            photoStreamContent.Headers.ContentDisposition =
                new ContentDispositionHeaderValue("form-data")
                {
                    Name = "File",
                    FileName = name + ".jpg"
                };
            photoStreamContent.Headers.ContentType =
                new MediaTypeHeaderValue("image/jpg");
            var form = new MultipartFormDataContent();
            form.Add(new StringContent(name), nameof(name));
            if (description != null)
            {
                form.Add(new StringContent(description), nameof(description));
            }
            if (latitude != null)
            {
                form.Add(new StringContent(latitude.ToString()), nameof(latitude));
            }
            if (longitude != null)
            {
                form.Add(new StringContent(longitude.ToString()), nameof(longitude));
            }
            form.Add(new StringContent(objectId.ToString()), nameof(objectId));
            form.Add(new StringContent(userId.ToString()), nameof(userId));
            if (photo != null)
            {
                form.Add(photoStreamContent, name + ".jpg");
            }

            return await PostImageAsync<Photo>(ApiUrl + "photo", GetServiceHeader(AccessToken), form, cancelToken);
        }

        public async Task<List<Tag>> GetTagsAsync(long imageId, CancellationToken cancelToken)
        {
            return await GetAsync<List<Tag>>(ApiUrl + "tag/" + imageId, GetServiceHeader(AccessToken), null, cancelToken);
        }

        public async Task<Tag> AddTagAsync(string name, long userId, long imageId, CancellationToken cancelToken)
        {
            JObject tag = JObject.FromObject(new
            {
                name,
                userId,
                imageId
            });
            return await PostAsync<Tag>(ApiUrl + "tag", GetServiceHeader(AccessToken), tag, cancelToken);
        }

        internal class ErrorModel
        {
            public string Error { get; set; }
            public string Message { get; set; }
        }

    }
}

