﻿using Mapsui;
using Mapsui.Geometries;
using Mapsui.Layers;
using Mapsui.UI.Forms;
using PhotoStruk.CustomElements;

namespace PhotoStruk.Services.Interfaces
{
    public interface IMapService
    {
        public CustomMapView MapView { get; set; }
        public ILayer Ilayer { get; set; }
        Map GetMap();
        void AddLayer(ILayer layer);
        void NavigateFromPosition(Position position);
        void NavigateFromPosition(Position position, int res);
        void NavigateFromPosition(Point point);
        void NavigateFromPosition(Point point, int res);
        void ClearLayers();
    }
}
