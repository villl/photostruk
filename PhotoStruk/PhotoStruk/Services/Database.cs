﻿using PhotoStruk.DeviceSpecificInterfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PhotoStruk.Services
{
    public class Database
    {
        #region Singleton
        private static Lazy<Database> _instance => new Lazy<Database>(() =>
            new Database(DependencyService.Get<IDeviceFile>().GetFilePath(Utils.Const.DbFileName)));
        public static SQLiteAsyncConnection Instance { get { return _instance.Value.connection; } }
        #endregion

        // Sqlite Db
        private readonly SQLiteAsyncConnection connection;
        private Database(string dbPath)
        {
            connection = new SQLiteAsyncConnection(dbPath);
        }
    }
}
