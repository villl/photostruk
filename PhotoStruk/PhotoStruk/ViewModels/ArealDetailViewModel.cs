﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.ViewModels.Base;

namespace PhotoStruk.ViewModels
{
    public class ArealDetailViewModel : BaseDetailViewModel<Areal>
    {
        public ArealDetailViewModel(Areal model, IUserDialogs dialogs) : base(model, dialogs)
        {

        }
    }
}
