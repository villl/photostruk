﻿using Acr.UserDialogs;
using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            Title = "About";

            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://xamarin.com/platform")));
        }

        public ICommand OpenWebCommand { get; }
    }
}