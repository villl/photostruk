﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.ViewModels
{
    public class GalleryViewModel : BaseViewModel
    {
        public List<Photo> Photos => MOCK.Photos;
        public GalleryViewModel(IUserDialogs dialogs) : base(dialogs)
        {

        }
    }
}
