﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoStruk.ViewModels
{
    public class ComponentDetailViewModel : BaseDetailViewModel<Component>
    {
        public ComponentDetailViewModel(Component model, IUserDialogs dialogs) : base(model, dialogs)
        {
            Title = Resources.R.Components;
        }

        //public async override Task LoadDataFromDbAsync()
        //{
        //    await this.Refresh();
        //}

        protected override Task Refresh()
        {
            return base.Refresh();
        }
    }
}
