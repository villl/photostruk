﻿using Acr.UserDialogs;
using PhotoStruk.Utils.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        public Command TileButtonClick => new Command<EPages>(async page => {
            this.Dialogs.Toast("Not implemented");
        });
        public HomePageViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            Title = Resources.R.PhotoStruk;
        }
    }
}
