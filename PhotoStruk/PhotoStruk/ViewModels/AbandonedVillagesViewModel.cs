﻿using Acr.UserDialogs;
using PhotoStruk.DeviceSpecificInterfaces;
using PhotoStruk.Models;
using PhotoStruk.Resources;
using PhotoStruk.Services;
using PhotoStruk.Utils;
using PhotoStruk.ViewModels.Base;
using PhotoStruk.Views;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class AbandonedVillagesViewModel : BaseViewModel
    {
        public DbService<Village> DbService => new DbService<Village>();

        private ObservableCollection<Village> villages;
        public ObservableCollection<Village> Villages
        {
            get 
            {
                // lazy loading
                if (villages == null)
                {
                    villages = new ObservableCollection<Village>();
                    return villages;
                }
                else
                {
                    return villages;
                }
            }
            set { villages = value; }
        }

        private Village selectedItem;

        public Village SelectedItem
        {
            get { return selectedItem; }
            set { SetProperty(ref selectedItem, value); }
        }

        public ICommand NavigateDetailCommand => new Command(async () =>
        {
            if (SelectedItem == null)
                return;

            await Shell.Current.Navigation.PushAsync(new VillageDetailPage(SelectedItem));
            SelectedItem = null;
        });

        public ICommand RefreshCommand => new Command(async () =>
        {
            await this.Refresh();
        });

        public AbandonedVillagesViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            Title = R.AbandonedVillages;
            LoadDataFromDbAsync();
        }

        private async Task LoadDataFromDbAsync()
        {
            await this.Refresh();
        }


        
        protected async Task Refresh()
        {
            using (this.Dialogs.Loading(R.Loading))
            {
                Villages = new ObservableCollection<Village>(DbService.GetItems());
            }
        }

    }
}
