using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.Resources;
using PhotoStruk.Services;
using PhotoStruk.Utils;
using PhotoStruk.ViewModels.Base;
using PhotoStruk.ViewModels.Interfaces;
using PhotoStruk.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class VillageDetailViewModel : BaseDetailViewModel<Village>, IWebViewModel
    {
        public ObservableCollection<Information> History { get; set; }
        public List<VillageImage> Images { get; set; } = new List<VillageImage>();
        public List<Photo> Photos { get; set; } = new List<Photo>();
        public List<Areal> Areals { get; set; } = new List<Areal>();

        private Information selectedItem;

        public Information SelectedItem
        {
            get { return selectedItem; }
            set { SetProperty(ref selectedItem, value); }
        }

        private VillageImage selectedImage;
        public VillageImage SelectedImage
        {
            get { return selectedImage; }
            set { SetProperty(ref selectedImage, value); }
        }

        private Photo selectedPhoto;
        public Photo SelectedPhoto
        {
            get { return selectedPhoto; }
            set { SetProperty(ref selectedPhoto, value); }
        }

        private Areal selectedAreal;

        public Areal SelectedAreal
        {
            get { return selectedAreal; }
            set { SetProperty(ref selectedAreal, value); }
        }


        private VillageModel selectedVillageModel;

        public VillageModel SelectedVillageModel
        {
            get { return selectedVillageModel; }
            set { SetProperty(ref selectedVillageModel, value); }
        }

        private int sellectedVillageHeightRequest = 0;

        public int SellectedVillageHeightRequest
        {
            get { return sellectedVillageHeightRequest; }
            set
            {
                SetProperty(ref sellectedVillageHeightRequest, value);
            }
        }


        public ICommand HistoryDetailCommand => new Command(async () =>
        {
            if (SelectedItem == null)
                return;

            if (SelectedItem.Detail == string.Empty)
            {
                SelectedItem = null;
                return;
            }

            await Shell.Current.Navigation.PushAsync(new HistoryDetailPage(SelectedItem));
            SelectedItem = null;
        });

        public ICommand GalleryCommand => new Command(async () =>
        {
            if (SelectedImage == null)
                return;

            await Shell.Current.Navigation.PushAsync(new GalleryPage());
            SelectedImage = null;
        });

        public ICommand GoToImageDetailCommand => new Command(async () =>
        {
            if (SelectedImage == null)
                return;

            await Shell.Current.Navigation.PushAsync(new PhotoDetailPage(SelectedImage, SelectedPhoto));
            SelectedImage = null;
        });

        public ICommand GoToPhotoDetailCommand => new Command(async () =>
        {
            if (SelectedPhoto == null)
                return;

            await Shell.Current.Navigation.PushAsync(new PhotoDetailPage(SelectedImage, SelectedPhoto));
            SelectedPhoto = null;
        });

        public ICommand GoToArealDetailCommand => new Command(async () =>
        {
            if (SelectedAreal == null)
            {
                return;
            }

            await Shell.Current.Navigation.PushAsync(new ArealDetailPage(SelectedAreal));
            SelectedAreal = null;
        });

        public ICommand GoToPhotoFormCommand => new Command(async () =>
        {
            await Shell.Current.Navigation.PushAsync(new PhotoFormPage(DetailModel.Id), true);
        });

        public CancellationTokenSource WebCancellationTokenSource => new CancellationTokenSource();

        public WebService WebService => new WebService(Dialogs);

        public VillageDetailViewModel(Village model, IUserDialogs dialogs) : base(model, dialogs)
        {
            Title = R.AbandonedVillages;
            LoadHistoryList(model?.History);
            LoadImagesAsync();
            LoadArealsAsync(model.Id);
            LoadPhotosAsync(model.Id);
            SelectVillageModel(model.NameCzech);
            //DetailModel = DbService.GetById(DetailModel.Id);
        }

        private async Task LoadImagesAsync()
        {
            using (this.Dialogs.Loading(R.Loading))
            {
                var db = new DbService<VillageImage>();
                List<VillageImage> villageImages = db.GetItemsWhere(x => x.VillageId == DetailModel.Id);

                foreach (var item in villageImages)
                {
                    item.Source = await WebService.GetImageAsync(item.Id, WebCancellationTokenSource.Token);
                   //Images.Add(item);
                }
                Images = villageImages;
                OnPropertyChanged(nameof(Images));
            }
        }

        public async Task LoadPhotosAsync(long objectId)
        {
            using (this.Dialogs.Loading(R.Loading))
            {

                List<Photo> photos = await WebService.GetPhotoImagesAsync(objectId, WebCancellationTokenSource.Token);

                if (photos != null)
                {
                    Photos = photos;
                    OnPropertyChanged(nameof(Photos));
                }
            }
        }

        private async Task LoadArealsAsync(long villageId)
        {
            DbService<Areal> dbService = new DbService<Areal>();
            Areals = dbService.GetItemsWhere(x => x.VillageId == villageId);
            OnPropertyChanged(nameof(Areals));
        }

        private void SelectVillageModel(string villageName)
        {
            SelectedVillageModel = App.InMemory.VillageModels.FirstOrDefault(x => x.Name == villageName);

            if (SelectedVillageModel != null)
            {
                SellectedVillageHeightRequest = 240;
            }
        }

        private void LoadHistoryList(History history)
        {
            List<Information> historyList = new List<Information>();
            if (history == null)
            {
                historyList.Add(new Information(R.MiddleAges, string.Empty));
                historyList.Add(new Information(R.EarlyModernAges, string.Empty));
                historyList.Add(new Information(R.NineteenthCentury, string.Empty));
                historyList.Add(new Information(R.StateBefore1945, string.Empty));
                historyList.Add(new Information(R.StateAfter1945, string.Empty));
                historyList.Add(new Information(R.State1989, string.Empty));
                historyList.Add(new Information(R.Present, string.Empty));
                History = new ObservableCollection<Information>(historyList);
            }
            else
            {

                historyList.Add(new Information(R.MiddleAges, history.MiddleAges));
                historyList.Add(new Information(R.EarlyModernAges, history.EarlyModernAges));
                historyList.Add(new Information(R.NineteenthCentury, history.NineteenthCentury));
                historyList.Add(new Information(R.StateBefore1945, history.StateBefore1945));
                historyList.Add(new Information(R.StateAfter1945, history.StateAfter1945));
                historyList.Add(new Information(R.State1989, history.State1989));
                historyList.Add(new Information(R.Present, history.Present));
                History = new ObservableCollection<Information>(historyList);
            }
        }

        //public async override Task LoadDataFromDbAsync()
        //{
        //    await this.Refresh();
        //}

        protected override Task Refresh()
        {
            return base.Refresh();
        }

        public Task LoadDataFromWebAsync()
        {
            throw new NotImplementedException();
        }
    }
}
