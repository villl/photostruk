﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using PhotoStruk.Models;
using PhotoStruk.Resources;
using PhotoStruk.Services;
using PhotoStruk.Utils;
using PhotoStruk.Views;
using Plugin.FacebookClient;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private CancellationToken CancellationToken => new CancellationTokenSource().Token;
        public WebService WebService => new WebService(UserDialogs.Instance);

        #region Properties
        private string email;
        public string Email
        {   
            get => email;
            set => SetProperty(ref email, value);
        }

        private string password;
        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }

        private bool isRemembered;

        public bool IsRemembered
        {
            get => isRemembered;
            set => SetProperty(ref isRemembered, value);
        }
        #endregion

        public ICommand LoginCommand => new Command(async () => await LoginCommandImpl());

        public ICommand NavigateRegisterCommand => new Command(async () =>
        {
            await Shell.Current.Navigation.PushModalAsync(new RegisterPage(), true);
        });

        public ICommand FacebookLoginCommand => new Command(async () =>
        {
            try
            {
                //var responseFB = await CrossFacebookClient.Current.LoginAsync(new string[] { "email" });
                var responseFB = await CrossFacebookClient.Current.RequestUserDataAsync(new string[] { "email", "first_name", "gender", "last_name" }, new string[] { "email" });

                //await Shell.Current.Navigation.PopToRootAsync();

                //Dialogs.Toast(responseFB.Data + responseFB.Message + responseFB.Status.ToString());

                if (responseFB.Status == FacebookActionStatus.Completed)
                {
                    var facebookProfile = JsonConvert.DeserializeObject<FacebookProfile>(responseFB.Data);
                    var signInResponse = await WebService.ExternalLoginAsync(facebookProfile.Email,
                        facebookProfile.FirstName, facebookProfile.LastName, CancellationToken);

                    if (signInResponse != null)
                    {
                        Preferences.Set(Const.AccessToken, signInResponse.Token);
                        Preferences.Set(Const.TokenExpiration, ReadTokedExpiration(signInResponse.Token));
                        Preferences.Set(Const.UserId, ReadTokedUserId(signInResponse.Token));
                        if (IsRemembered)
                        {
                            Preferences.Set(Const.Email, Email);
                        }
                        else
                        {
                            Preferences.Remove(Const.Email);
                        }
                        await Shell.Current.Navigation.PopModalAsync();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        });
        public ICommand ToggleSwitch => new Command(() => {
            IsRemembered = !IsRemembered;
        });

        private async Task LoginCommandImpl()
        {
            using (this.Dialogs.Loading(R.Loading))
            {
                string errorMessage = ValidateLogin();
                if (errorMessage.Length > 0)
                {
                    await Dialogs.AlertAsync(errorMessage, R.Error, R.Ok);
                    return;
                }

                var signInResponse = await WebService.LoginAsync(Email, Password, CancellationToken);

                if (signInResponse != null)
                {
                    Preferences.Set(Const.AccessToken, signInResponse.Token);
                    Preferences.Set(Const.TokenExpiration, ReadTokedExpiration(signInResponse.Token));
                    Preferences.Set(Const.UserId, ReadTokedUserId(signInResponse.Token));
                    if (IsRemembered)
                    {
                        Preferences.Set(Const.Email, Email);
                    }
                    else
                    {
                        Preferences.Remove(Const.Email);
                    }
                    await Shell.Current.Navigation.PopModalAsync();
                }
            }
        }

        private string ValidateLogin()
        {
            StringBuilder sb = new StringBuilder();

            if (string.IsNullOrWhiteSpace(Email))
            {
                sb.AppendLine(string.Format(R.EmptyField, R.Email));
            }

            if (!string.IsNullOrWhiteSpace(Email))
            {
                if (!Regex.Match(Email, Const.EmailRegex).Success)
                {
                    sb.AppendLine(R.ErrorEmail);
                }
            }

            if (string.IsNullOrWhiteSpace(Password))
            {
                sb.AppendLine(string.Format(R.EmptyField, R.Password));
            }

            return sb.ToString();
        }

        private DateTime ReadTokedExpiration(string jwt)
        {
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            return token.ValidTo;
        }

        private long ReadTokedUserId(string jwt)
        {
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            if (token.Payload.TryGetValue(Const.UserId, out object result))
            {
                return long.Parse(result.ToString()); 
            }
            else
            {
                Dialogs.Toast("nepodařilo se nalézt token");
                return 0L;
            }
        }

        

        public LoginViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            Title = R.Login;
            if (Preferences.Get(Const.Email, string.Empty) != string.Empty)
            {
                Email = Preferences.Get(Const.Email, string.Empty);
                IsRemembered = true;
            }
        }
    }
}
