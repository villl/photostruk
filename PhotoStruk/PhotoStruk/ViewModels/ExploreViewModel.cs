﻿using Mapsui.UI.Forms;
using PhotoStruk.DeviceSpecificInterfaces;
using PhotoStruk.Models;
using PhotoStruk.Resources;
using PhotoStruk.Services;
using PhotoStruk.Utils;
using PhotoStruk.Utils.Enums;
using PhotoStruk.ViewModels.Base;
using PhotoStruk.Views;
using Point = Mapsui.Geometries.Point;
using Map = Mapsui.Map;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Mapsui;
using PhotoStruk.CustomElements;
using PhotoStruk.Services.Interfaces;
using PhotoStruk.Models.Interfaces;
using System.Diagnostics;
using Xamarin.Essentials;
using Mapsui.Projection;
using Acr.UserDialogs;
using Realms;

namespace PhotoStruk.ViewModels
{
    public enum EPinType
    {
        Village,
        Component,
        Photograph
    }

    public class ExploreViewModel : BaseViewModel
    {
        #region Property
        public List<Photo> Photos => MOCK.Photos;

        public List<Village> VillageItems = new List<Village>();
        public List<Component> ComponentItems = new List<Component>();
        public List<Photograph> PhotographItems = new List<Photograph>();
        public List<VillageImage> VillageImagesItems = new List<VillageImage>();

        private DbService<Village> DbVillage = new DbService<Village>();
        private DbService<Component> DbComponent = new DbService<Component>();
        private DbService<VillageImage> DbVillageImage = new DbService<VillageImage>();

        private Village selectedVillage;

        public Village SelectedVillage
        {
            get { return selectedVillage; }
            set { SetProperty(ref selectedVillage, value); }
        }

        private Component selectedComponent;

        public Component SelectedComponent
        {
            get { return selectedComponent; }
            set { SetProperty(ref selectedComponent, value); }
        }

        private Photograph selectedPhotograph;

        public Photograph SelectedPhotograph
        {
            get { return selectedPhotograph; }
            set { SetProperty(ref selectedPhotograph, value); }
        }


        private string titleAlert;

        public string TitleAlert
        {
            get { return titleAlert; }
            set { SetProperty(ref titleAlert, value); }
        }

        private string detailAlert;

        public string DetailAlert
        {
            get { return detailAlert; }
            set { SetProperty(ref detailAlert, value); }
        }

        private EPinType type;

        public EPinType Type
        {
            get { return type; }
            set { SetProperty(ref type, value); }
        }

        private Map map;
        private CustomMapView mapView;
        private readonly MapService mapService;

        public Map Map
        {
            get => map;
            set => SetProperty(ref map, value);
        }

        public CustomMapView MapView
        {
            get => mapView;
            set => SetProperty(ref mapView, value);
        }
        #endregion

        #region Commands
        public ICommand PinCommand => new Command<Pin>(async (pin) =>
        {
            if (pin.Color.Equals(Color.DarkOrange))
            {

                SelectedVillage = VillageItems.FirstOrDefault(x => x.LabelId == pin.Label);
                if (SelectedVillage == null)
                {
                    await this.Dialogs.AlertAsync("Village not found", R.Error);
                    return;
                }
                TitleAlert = SelectedVillage.NameCzech;
                DetailAlert = SelectedVillage.District;
                Type = EPinType.Village;
            }
            else if (pin.Color.Equals(Color.Blue))
            {
                SelectedComponent = ComponentItems.FirstOrDefault(x => x.LabelId == pin.Label);
                if (SelectedComponent == null)
                {
                    await this.Dialogs.AlertAsync(R.Error, "Component not found");
                    return;
                }
                TitleAlert = SelectedComponent.Name;
                DetailAlert = SelectedComponent.Notice;
                Type = EPinType.Component;
            }
            else if (pin.Color.Equals(Color.Red))
            {
                SelectedPhotograph = PhotographItems.FirstOrDefault(x => x.LabelId == pin.Label);
                if (SelectedPhotograph == null)
                {
                    await this.Dialogs.AlertAsync("Photograph not found", R.Error);
                    return;
                }
                TitleAlert = SelectedPhotograph.Location;
                DetailAlert = SelectedPhotograph.TimePeriod;
                Type = EPinType.Photograph;
            }
        });

        /// <summary>
        /// Show search filter
        /// </summary>
        public ICommand SearchCommand => new Command(() =>
        {
            Shell.Current.Navigation.PushAsync(new SearchPage());
        });

        public ICommand LocationCommand => new Command(async () =>
        {
            //mapService.NavigateFromPosition(SphericalMercator.FromLonLat(16.7936, 49.7636), 5);
            //MapView.Navigator.CenterOn(49.7636, 16.7936);

            var location = await LocationHelper.GetCurrentLocationAsync();

            if (location == null)
            {
                this.Dialogs.Toast(R.LocationNotFound);
            }
            else
            {
                //MapView.MyLocationLayer.UpdateMyLocation(new Position(location.Latitude, location.Longitude));
                //MapView.Navigator.CenterOn(location.Latitude, location.Longitude);
                // TODO: position is not changing
                //var startPoint = SphericalMercator.FromLonLat(location.Longitude, location.Latitude);
                //map.Home = n => n.NavigateTo(startPoint, Map.Resolutions[5]);
            }
        });

        /// <summary>
        /// Navigate to detail
        /// </summary>
        public ICommand DetailCommand => new Command(async () =>
        {
            switch (Type)
            {
                case EPinType.Village:
                    await Shell.Current.Navigation.PushAsync(new VillageDetailPage(SelectedVillage));
                    break;
                case EPinType.Component:
                    await Shell.Current.Navigation.PushAsync(new ComponentDetailPage(SelectedComponent));
                    break;
                case EPinType.Photograph:
                    //await Shell.Current.Navigation.PushAsync(new PhotoDetailPage(SelectedPhotograph));
                    break;
                default:
                    break;
            }
        });
        #endregion

        public ExploreViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            this.mapService = new MapService();
            this.LoadDataFromDbAsync();
            this.InitializeMap();


            Title = R.Explore;
        }

        private void InitializeMap()
        {
            Map = mapService.GetMap();



            //mapView.MyLocationLayer.UpdateMyLocation(new Mapsui.UI.Forms.Position());

            //Action<IMapControl> setup = null;
            //Func<MapView, MapClickedEventArgs, bool> c = null;
            //setup(mapView);
            //Clicker = c;
        }

        /// <summary>
        /// Load pins on map
        /// </summary>
        public async void InitializePins()
        {
            //mapView.Pins.Add(CreatePin(mapView, VillageItems.First()));
            //mapView.Pins.Add(CreatePin(mapView, MOCK.Components.First()));
            //mapView.Pins.Add(CreatePin(mapView, new Photograph() { Category = "zrní", Latitude = 14.600, Longitude = 49.624}));

            //VillageItems.Select(v => CreatePin(mapView, v));
            //ComponentItems.Select(k => CreatePin(mapView, k));
            //PhotographItems.Select(p => CreatePin(mapView, p));

            using (this.Dialogs.Loading(R.Loading))
            {
                // TODO: Run on bg thread
                //await Task.Run(() =>
                //       {
                if (Preferences.Get(Const.VillagesIsToggled, true))
                {
                    string search = Preferences.Get(Const.Search, string.Empty);
                    if (string.IsNullOrEmpty(search))
                    {
                        VillageItems = DbVillage.GetItems();
                    }
                    else
                    {
                        VillageItems = DbVillage.GetItemsFilter($"NameCzech CONTAINS '{search}'");
                    }
                    
                    foreach (var model in VillageItems)
                    {
                        mapView.Pins.Add(CreatePin(mapView, model));
                    }
                }

                // });

                //await Task.Run(() =>
                //{

                if (Preferences.Get(Const.ComponentsIsToggled, true))
                {
                    ComponentItems = DbComponent.GetItems();
                    foreach (var model in ComponentItems.Take(500))
                    {
                        if (model.Latitude != null && model.Longitude != null)
                        {
                            mapView.Pins.Add(CreatePin(mapView, model));
                        }
                    }
                }

                //});

                //await Task.Run(() =>
                //{
                //    foreach (var model in PhotographItems)
                //    {
                //        mapView.Pins.Add(CreatePin(mapView, model));
                //    }
                //});
            }
        }

        public async void ClearPins()
        {
            mapView.Pins.Clear();
        }

        private async Task LoadDataFromDbAsync()
        {
            await this.Refresh();
        }

        private async Task Refresh()
        {

            using (this.Dialogs.Loading(R.Loading))
            {

                //ComponentItems = MOCK.Components;
                PhotographItems = MOCK.Photographs;
            }
        }

        private Pin CreatePin(MapView mapView, IPinable model)
        {
            var pinColor = Color.Black;

            if (model is Village)
            {
                pinColor = Color.DarkOrange;
            }
            else if (model is Component)
            {
                pinColor = Color.Blue;
            }
            else if (model is Photograph)
            {
                pinColor = Color.Red;
            }
            if (model.Latitude == null || model.Longitude == null)
            {
                return null;
            }

            Position pinPosition = new Position((double)model.Latitude, (double)model.Longitude);

            return new Pin(mapView)
            {
                Type = PinType.Pin,
                Scale = 1,
                Color = pinColor,
                Label = model.LabelId,
                Position = pinPosition,
                Address = "Adress",
                Svg = "pin.svg"
            };

        }


    }
}
