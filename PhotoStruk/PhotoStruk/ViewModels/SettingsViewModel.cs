﻿using Acr.UserDialogs;
using PhotoStruk.DeviceSpecificInterfaces;
using PhotoStruk.Models;
using PhotoStruk.Resources;
using PhotoStruk.Utils;
using PhotoStruk.Utils.Enums;
using PhotoStruk.ViewModels.Base;
using PhotoStruk.Views;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class SettingsViewModel : BaseWebViewModel
    {
        public ObservableCollection<string> LanguageList { get; set; } = new ObservableCollection<string>
        {
            ELanguageType.EN.ToString(),
            ELanguageType.CZ.ToString(), 
            ELanguageType.DE.ToString()
        };

        string selectedLanguage = string.Empty;
        public string SelectedLanguage 
        { 
            get { return selectedLanguage; }
            set { SetProperty(ref selectedLanguage, value); }
        }
        public string AppVersion => AppInfo.VersionString;
        public string BuildVersion => AppInfo.BuildString;
        public string CultureInfo 
        {
            get { return selectedLanguage; }
            set { SetProperty(ref selectedLanguage, value); }
        }

        private User currentUser;

        public User CurrentUser
        {
            get { return currentUser; }
            set { SetProperty(ref currentUser, value); }
        }

        public ICommand GotoLoginCommand => new Command(async () =>
        {
            await Shell.Current.Navigation.PushModalAsync(new LoginPage());
        });

        public ICommand LogoutCommand => new Command(() =>
        {
            Preferences.Clear();
        });

        public SettingsViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            Title = R.Settings;
            CultureInfo = DependencyService.Get<ILocalize>().GetCultureInfo().ToString();
            LoadDataFromWebAsync();
        }

        public void ChangeLanguage(string language)
        {
            switch (language)
            {
                case "EN": SetCultureInfo("en"); break;
                case "CZ": SetCultureInfo("cs"); break;
                case "DE": SetCultureInfo("de"); break;
            }
        }

        void SetCultureInfo(string language)
        {
            CultureInfo ci = new CultureInfo(language);
            R.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocate(ci);
        }

        private bool IsUserLogged()
        {
            if (Preferences.Get(Const.TokenExpiration, DateTime.MinValue) <= DateTime.Now)
            {
                Preferences.Clear();
                Shell.Current.Navigation.PushModalAsync(new LoginPage());
                return false;
            }
            return true;
        }

        public async override Task LoadDataFromWebAsync()
        {
            if (IsUserLogged())
            {
                User user = await WebService.GetUserAsync
                        (Preferences.Get(Const.UserId, 0L), WebCancellationTokenSource.Token)
                        .ConfigureAwait(false);

                if (user != null)
                {
                    CurrentUser = user;
                }
            }

            
        }
    }
}
