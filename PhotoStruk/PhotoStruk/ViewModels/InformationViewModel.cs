﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.ViewModels
{
    public class InformationViewModel : BaseViewModel
    {
        public InformationViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            Title = Resources.R.Information;
        }
    }
}
