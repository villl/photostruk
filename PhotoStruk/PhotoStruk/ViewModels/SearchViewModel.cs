﻿using Acr.UserDialogs;
using PhotoStruk.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class SearchViewModel : BaseViewModel
    {
        #region Property
        private string search;
        public string Search
        {
            get => search;
            set => SetProperty(ref search, value);
        }

        private bool photographsIsToggled;
        public bool PhotographsIsToggled
        {
            get => photographsIsToggled;
            set => SetProperty(ref photographsIsToggled, value);
        }

        private bool villagesIsToggled;
        public bool VillagesIsToggled {
            get => villagesIsToggled;
            set => SetProperty(ref villagesIsToggled, value);
        }

        private bool componentsIsToggled;
        public bool ComponentsIsToggled {
            get => componentsIsToggled;
            set => SetProperty(ref componentsIsToggled, value);
        }

        private bool landscapeIsToggled;
        public bool LandscapeIsToggled {
            get => landscapeIsToggled;
            set => SetProperty(ref landscapeIsToggled, value);
        }

        private bool buildingIsToggled;
        public bool BuildingIsToggled {
            get { return buildingIsToggled; }
            set { SetProperty(ref buildingIsToggled, value); }
        }

        private bool groupIsToggled;
        public bool GroupIsToggled {
            get => groupIsToggled;
            set => SetProperty(ref groupIsToggled, value);
        }

        private bool sceneIsToggled;
        public bool SceneIsToggled {
            get => sceneIsToggled;
            set => SetProperty(ref sceneIsToggled, value);
        }

        private bool postcardIsToggled;
        public bool PostcardIsToggled {
            get => postcardIsToggled;
            set => SetProperty(ref postcardIsToggled, value);
        }

        private bool portraitIsToggled;
        public bool PortraitIsToggled {
            get => portraitIsToggled;
            set => SetProperty(ref portraitIsToggled, value);
        }

        private bool statueIsToggled;
        public bool StatueIsToggled {
            get => statueIsToggled;
            set => SetProperty(ref statueIsToggled, value);
        }

        private bool otherIsToggled;
        public bool OtherIsToggled {
            get => otherIsToggled;
            set => SetProperty(ref otherIsToggled, value);
        }

        private int fromYear;
        public int FromYear
        {
            get => fromYear;
            set => SetProperty(ref fromYear, value);
        }

        private int toYear;
        public int ToYear
        {
            get => toYear;
            set => SetProperty(ref toYear, value);
        }

        #endregion

        public ICommand SearchCommand => new Command(async () => 
        {
            Preferences.Set(Const.Search, Search);
            Preferences.Set(Const.VillagesIsToggled, VillagesIsToggled);
            Preferences.Set(Const.ComponentsIsToggled, ComponentsIsToggled);
            Preferences.Set(Const.LandscapeIsToggled, LandscapeIsToggled);
            Preferences.Set(Const.BuildingIsToggled, BuildingIsToggled);
            Preferences.Set(Const.GroupIsToggled, GroupIsToggled);
            Preferences.Set(Const.SceneIsToggled, SceneIsToggled);
            Preferences.Set(Const.PostcardIsToggled, PostcardIsToggled);
            Preferences.Set(Const.PortraitIsToggled, PortraitIsToggled);
            Preferences.Set(Const.StatueIsToggled, StatueIsToggled);
            Preferences.Set(Const.FromYear, FromYear);
            Preferences.Set(Const.ToYear, ToYear);
            await Shell.Current.Navigation.PopAsync(true);
        });

        private void InitFilter()
        {
            Preferences.Get(Const.Search, string.Empty);
            VillagesIsToggled = Preferences.Get(Const.VillagesIsToggled, true);
            ComponentsIsToggled = Preferences.Get(Const.ComponentsIsToggled, true);
            Preferences.Get(Const.LandscapeIsToggled, true);
            Preferences.Get(Const.BuildingIsToggled, true);
            Preferences.Get(Const.GroupIsToggled, true);
            Preferences.Get(Const.SceneIsToggled, true);
            Preferences.Get(Const.PostcardIsToggled, true);
            Preferences.Get(Const.PortraitIsToggled, true);
            Preferences.Get(Const.StatueIsToggled, true);
            Preferences.Get(Const.FromYear, 1900);
            Preferences.Get(Const.ToYear, 2000);
        }

        public SearchViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            Title = Resources.R.Search;
            FromYear = 1900;
            ToYear = 2000;
            this.InitFilter();
        }   
    }
}

