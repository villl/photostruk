﻿using Acr.UserDialogs;
using PhotoStruk.Services;
using PhotoStruk.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoStruk.ViewModels.Base
{
    public abstract class BaseWebViewModel : BaseViewModel, IWebViewModel
    {
        public CancellationTokenSource WebCancellationTokenSource => new CancellationTokenSource();
        public WebService WebService => new WebService(Dialogs);
        public abstract Task LoadDataFromWebAsync();

        public BaseWebViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            LoadDataFromWebAsync();
        }
    }
}
