﻿using Acr.UserDialogs;
using PhotoStruk.Models.Interfaces;
using PhotoStruk.Services;
using PhotoStruk.ViewModels.Interfaces;
using Realms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels.Base
{
    public abstract class BaseDbViewModel<TEntity> : BaseViewModel, IDbViewModel<TEntity> where TEntity : RealmObject, IEntityModel, new()
    {
        public Type DetailViewType { get; set; }
        public DbService<TEntity> DbService => new DbService<TEntity>();

        public ICommand ShowDetailCommand => new Command<object>(async model => await ShowDetailCommandImpl(model));

        /// <summary>
        /// For show only one detail
        /// </summary>
        protected bool IsDetailShowing = false;

        protected async Task ShowDetailCommandImpl(object model)
        {

            if (IsDetailShowing) return;
            IsDetailShowing = true;

            await Task.Delay(750); // Prevent multi click while detail is loading
            IsDetailShowing = false;
        }

        public abstract Task LoadDataFromDbAsync();

        public List<TEntity> AllEntityItems = new List<TEntity>();
        public IEnumerable<TEntity> GetAllEntityItems => AllEntityItems;

        public BaseDbViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            LoadDataFromDbAsync();
        }
    }
}
