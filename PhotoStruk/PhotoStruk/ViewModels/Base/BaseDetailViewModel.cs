﻿using Acr.UserDialogs;
using PhotoStruk.Models.Interfaces;
using PhotoStruk.Services;
using PhotoStruk.ViewModels.Interfaces;
using Realms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels.Base
{
    public class BaseDetailViewModel<TDetailModel> : BaseViewModel, IDetailViewModel where TDetailModel : RealmObject, IEntityModel, new()
    {
        public DbService<TDetailModel> DbService => new DbService<TDetailModel>();
        private readonly TDetailModel _oldModel;

        /// <summary>
        /// Check if is create new item (no update)
        /// </summary>
        public bool IsCreateNew { get; private set; }

        /// <summary>
        /// Check if edit function is enabled (Create, Update, Delete)
        /// </summary>
        public bool IsEditable { get; set; } = true;

        /// <summary>
        /// Detail model
        /// </summary>
        public TDetailModel DetailModel { get; set; }

        public BaseDetailViewModel(TDetailModel model, IUserDialogs dialogs) : base(dialogs)
        {
            Title = Resources.R.Detail;
            DetailModel = DbService.GetById(model.Id);
            //DbService.GetById(model.Id);
            // Set create new
            //IsCreateNew = model == null;

            //_dbService = dbService ?? throw new ArgumentNullException(nameof(dbService));
            //_oldModel = model == null ? new TDetailModel() : dbService.GetById(model.Id).Result;
            //// Clone object
            //DetailModel = (TDetailModel)_oldModel.Clone();
        }

        #region Commands
        /// <summary>
        /// Delete model command
        /// </summary>
        public ICommand DeleteCommand => new Command<TDetailModel>(async model => await DeleteCommandImpl(model ?? DetailModel));
        protected virtual async Task DeleteCommandImpl(TDetailModel model)
        {
            if (!IsEditable) return;  // Editable Check

            //await DbService.DeleteItemAsync(model);
            //RefreshPage(Utils.ERefreshType.PopModalAndRefresh);
        }
        /// <summary>
        /// Save model command (Create, Update)
        /// </summary>
        public ICommand SaveCommand => new Command<TDetailModel>(async model => await SaveCommandImpl(model ?? DetailModel));
        protected virtual async Task SaveCommandImpl(TDetailModel model)
        {
            if (!IsEditable) return; // Editable Check

            //await DbService.SaveItemAsync(model);
            //RefreshPage(Utils.ERefreshType.PopModalAndRefresh);
        }
        /// <summary>
        /// Undo mode changes command
        /// </summary>
        public ICommand UndoCommand => new Command(async () => await UndoCommandImpl());
        protected virtual Task UndoCommandImpl()
        {
            // No need for Editable Check 
            //RefreshPage(Utils.ERefreshType.PopModalAndRefresh);
            return Task.CompletedTask;
        }
        /// <summary>
        /// BackButton pressed command
        /// </summary>
        public ICommand BackButtonCommand => new Command(async () => await BackButtonCommandImpl());
        protected virtual async Task BackButtonCommandImpl()
        {
            if (IsEditable && !DetailModel.Equals(_oldModel))
            {
                // Alert with response ("Zahodit" = OK button)
                if (await this.Dialogs.ConfirmAsync(Resources.R.ChangeDetected, Resources.R.Warning, Resources.R.ContinueChanges, Resources.R.RemoveChanges))
                {
                    return;
                }
            }
            UndoCommand.Execute(null);
        }
        /// <summary>
        /// Options pressed (Update options)
        /// </summary>
        public ICommand OptionsCommand => new Command(async () => await OptionsCommandImpl());
        protected virtual async Task OptionsCommandImpl()
        {
            if (!IsEditable) return; // Editable Check

            var result = await this.Dialogs.ConfirmAsync(Resources.R.Options, Resources.R.Cancel, Resources.R.SaveChanges );
            if (result)
                return;

            // Delete
            if (result.Equals(Resources.R.Delete))
            {
                if (await this.Dialogs.ConfirmAsync(Resources.R.AreYouSure, Resources.R.Delete, Resources.R.Yes, Resources.R.No))
                {
                    DeleteCommand.Execute(null);
                }
            }
            // Save
            if (result.Equals(Resources.R.SaveChanges))
            {
                SaveCommand.Execute(null);
            }
        }

        #endregion

        Type detailPageType = null;
        /// <summary>
        /// Detail page
        /// </summary>
        public Type DetailPageType
        {
            get { return detailPageType; }
            set { SetProperty(ref detailPageType, value); }
        }
    }
}
