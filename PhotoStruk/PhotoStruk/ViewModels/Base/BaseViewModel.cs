﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using PhotoStruk.Models;
using PhotoStruk.Services;
using System.Threading.Tasks;
using PhotoStruk.Utils;
using Acr.UserDialogs;

namespace PhotoStruk.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        protected IUserDialogs Dialogs { get; }

        bool isLandscape = false;
        /// <summary>
        /// Landscape orientation
        /// </summary>
        public bool IsLandscape
        {
            get { return isLandscape; }
            set { SetProperty(ref isLandscape, value); }
        }

        bool isPortrait = true;
        /// <summary>
        /// Portrait orientation
        /// </summary>
        public bool IsPortrait
        {
            get { return isPortrait; }
            set { SetProperty(ref isPortrait, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        /// <summary>
        /// Override if something need refreshing (navigation or command)
        /// </summary>
        /// <param name="filter">Text to filter</param>
        /// <returns></returns>
        protected virtual Task Refresh()
        {
            return Task.CompletedTask;
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public BaseViewModel(IUserDialogs dialogs)
        {
            this.Dialogs = dialogs;
        }
    }
}
