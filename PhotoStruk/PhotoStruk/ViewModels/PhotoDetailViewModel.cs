﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.Resources;
using PhotoStruk.Services;
using PhotoStruk.Utils;
using PhotoStruk.ViewModels.Base;
using PhotoStruk.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class PhotoDetailViewModel : BaseViewModel
    {
        public DbService<VillageImage> DbService => new DbService<VillageImage>();
        public VillageImage DetailModel { get; set; }
        private ObservableCollection<Comment> comments;
        public ObservableCollection<Comment> Comments
        {
            get
            {
                // lazy loading
                if (comments == null)
                {
                    comments = new ObservableCollection<Comment>();
                    return comments;
                }
                else
                {
                    return comments;
                }
            }
            set { comments = value; }
        }
        //public ObservableCollection<Tag> Tags = new ObservableCollection<Tag>();
        public CancellationTokenSource WebCancellationTokenSource => new CancellationTokenSource();
        public WebService WebService => new WebService(Dialogs);

        public long ObjectId { get; set; }

        private string commentText;

        public string CommentText
        {
            get { return commentText; }
            set { SetProperty(ref commentText, value); }
        }


        private string tagText;

        public string TagText
        {
            get { return tagText; }
            set { SetProperty(ref tagText, value); }
        }


        public PhotoDetailViewModel(VillageImage villageImage, Photo photo, IUserDialogs dialogs) : base(dialogs)
        {
            Title = R.Photo;
            if (villageImage != null)
            {
                ObjectId = villageImage.Id;
                DetailModel = villageImage;
                LoadCommentsAsync(villageImage.Id);
                LoadTagsAsync(villageImage.Id);
                LoadImageAsync(villageImage.Id);
            }
            else if (photo != null)
            {
                ObjectId = photo.Id;
                LoadCommentsAsync(photo.Id);
                LoadTagsAsync(photo.Id);
                Latitude = photo.Latitude;
                Longitude = photo.Longitude;
                SelectedImage = new Uri(photo.Url);
            }
            
            
        }

        private ImageSource selectedImage;
        public ImageSource SelectedImage {
            get { return selectedImage; }
            set { SetProperty(ref selectedImage, value); }
        }

        private double latitude;
        public double Latitude
        {
            get { return latitude; }
            set { SetProperty(ref latitude, value); }
        }

        private double longitude;
        public double Longitude
        {
            get { return longitude; }
            set { SetProperty(ref longitude, value); }
        }

        public ICommand AddTagCommand => new Command(async () =>
        {
            if (!IsUserLogged())
            {
                return;
            }

            var result = await Dialogs.PromptAsync(R.TagName, R.AddTag, R.Add, R.Cancel, "Tag", InputType.Name);

            if (result.Ok)
            {
                if (string.IsNullOrWhiteSpace(result.Value))
                {
                    this.Dialogs.Toast(R.EmptyString);
                    return;
                }

                Tag tag = await WebService.AddTagAsync(result.Value, Preferences.Get(Const.UserId, 0L), ObjectId, WebCancellationTokenSource.Token);

                if (tag != null)
                {                   
                    List<Tag> tags = new List<Tag>();
                    tags.Add(tag);

                    Dialogs.Toast(R.TagAdded);
                    MessagingCenter.Send(this, "SendTags", tags);
                }
            }
        });

        public ICommand AddLikeCommand => new Command<Comment>(async (comment) =>
        {
            if (!IsUserLogged())
            {
                return;
            }

            if (comment == null)
            {
                return;
            }

            Comment resultComment = await WebService.LikeAsync(true, Preferences.Get(Const.UserId, 0L), comment.Id, WebCancellationTokenSource.Token);

            if (resultComment != null)
            {
                int index = Comments.IndexOf(comment);
                Comments.RemoveAt(index);
                Comments.Insert(index, resultComment);
                OnPropertyChanged(nameof(Comments));
            }
        });

        public ICommand AddDislikeCommand => new Command<Comment>(async (comment) =>
        {
            
            if (!IsUserLogged())
            {
                return;
            }

            if (comment == null)
            {
                return;
            }

            Comment resultComment = await WebService.LikeAsync(false, Preferences.Get(Const.UserId, 0L), comment.Id, WebCancellationTokenSource.Token);

            if (resultComment != null)
            {
                int index = Comments.IndexOf(comment);
                Comments.RemoveAt(index);
                Comments.Insert(index, resultComment);
                OnPropertyChanged(nameof(Comments));
            }
        });

        public ICommand AddCommentCommand => new Command(async () =>
        {
            if (!IsUserLogged())
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.CommentText))
            {
                this.Dialogs.Toast(R.EmptyString);
                return;
            }

            Comment comment = await WebService.AddCommentAsync(CommentText, Preferences.Get(Const.UserId, 0L), ObjectId, WebCancellationTokenSource.Token);

            if (comment != null)
            {
                Comments.Add(comment);
                OnPropertyChanged(nameof(Comments));
                CommentText = string.Empty;
                Dialogs.Toast(R.CommentAdded);
            }
        });

        public ICommand EditCommentCommand => new Command<Comment>(async (comment) => 
        {
            if (!IsUserLogged())
            {
                return;
            }

            if (comment == null)
            {
                return;
            }

            var result = await Dialogs.PromptAsync(R.TagName, R.AddComment, R.Edit, R.Cancel, R.Comment, InputType.Name);

            if (result.Ok)
            {
                if (string.IsNullOrWhiteSpace(result.Value))
                {
                    this.Dialogs.Toast(R.EmptyString);
                    return;
                }

                Comment commentResult = await WebService.EditCommentAsync(result.Value, comment.Id, WebCancellationTokenSource.Token);
                
                if (commentResult != null)
                {
                    int index = Comments.IndexOf(comment);
                    Comments.RemoveAt(index);
                    Comments.Insert(index, commentResult);
                    OnPropertyChanged(nameof(Comments));
                }
            }
        });

        public ICommand DeleteCommentCommand => new Command<Comment>(async (comment) => 
        {
            if (!IsUserLogged())
            {
                return;
            }

            if (comment == null)
            {
                return;
            }

            if (await Dialogs.ConfirmAsync(R.AreYouSure, null, R.Delete, R.Cancel))
            {
                Comment commentResult = await WebService.DeleteCommentAsync(comment.Id, WebCancellationTokenSource.Token);

                if (commentResult != null)
                {
                    Comments.Remove(comment);
                    OnPropertyChanged(nameof(Comments));
                }
            }
        });

        public async Task LoadCommentsAsync(long imageId)
        {
            using (this.Dialogs.Loading(R.Loading))
            {
                Comments = new ObservableCollection<Comment>
                    (await WebService.GetCommentImagesAsync(imageId, WebCancellationTokenSource.Token));
                OnPropertyChanged(nameof(Comments));
            }
        }

        public async Task LoadTagsAsync(long imageId)
        {
            using (this.Dialogs.Loading(R.Loading))
            {
                List<Tag> tags = await WebService.GetTagsAsync(imageId, WebCancellationTokenSource.Token);
                MessagingCenter.Send(this, "SendTags", tags);
            }
        }

        public async Task LoadImageAsync(long imageId)
        {
            SelectedImage = await WebService.GetImageAsync(imageId, WebCancellationTokenSource.Token);

        }

        private bool IsUserLogged()
        {
            if (Preferences.Get(Const.TokenExpiration, DateTime.MinValue) <= DateTime.Now)
            {
                Preferences.Clear();
                Shell.Current.Navigation.PushModalAsync(new LoginPage());
                return false;
            }
            return true;
        }

        protected override Task Refresh()
        {
            return base.Refresh();
        }
    }
}
