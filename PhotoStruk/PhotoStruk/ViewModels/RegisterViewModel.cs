﻿using Acr.UserDialogs;
using PhotoStruk.Resources;
using PhotoStruk.Services;
using PhotoStruk.Utils;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        private CancellationToken CancellationToken => new CancellationTokenSource().Token;
        public WebService WebService => new WebService(UserDialogs.Instance);

        #region Properties
        private string firstName;
        public string FirstName
        {
            get => firstName;
            set => SetProperty(ref firstName, value);
        }

        private string lastName;
        public string LastName
        {
            get => lastName;
            set => SetProperty(ref lastName, value);
        }

        private string email;
        public string Email
        {
            get => email;
            set => SetProperty(ref email, value);
        }

        private string password;
        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }
        #endregion

        public ICommand RegisterCommand => new Command(async () =>
        {
            using (this.Dialogs.Loading(R.Loading))
            {
                string errorMessage = ValidateRegister();
                if (errorMessage.Length > 0)
                {
                    await Dialogs.AlertAsync(errorMessage, R.Error, R.Ok);
                    return;
                }

                var registerResponse = await WebService.RegisterAsync(Email, Password, FirstName, LastName, CancellationToken);

                if (registerResponse != null)
                {
                    Preferences.Set(Const.AccessToken, registerResponse.Token);
                    Preferences.Set(Const.TokenExpiration, ReadTokedExpiration(registerResponse.Token));
                    Preferences.Set(Const.UserId, ReadTokedUserId(registerResponse.Token));

                    await Shell.Current.Navigation.PopModalAsync();
                }
            }
        });

        public ICommand BackCommand => new Command(async () =>
        {
            await Shell.Current.Navigation.PopModalAsync();
        });

        private string ValidateRegister()
        {
            StringBuilder sb = new StringBuilder();

            if (string.IsNullOrWhiteSpace(FirstName))
            {
                sb.AppendLine(string.Format(R.EmptyField, R.FirstName));
            }

            if (string.IsNullOrWhiteSpace(LastName))
            {
                sb.AppendLine(string.Format(R.EmptyField, R.LastName));
            }

            if (string.IsNullOrWhiteSpace(Email))
            {
                sb.AppendLine(string.Format(R.EmptyField, R.Email));
            }

            if (!string.IsNullOrWhiteSpace(Email))
            {
                if (!Regex.Match(Email, Const.EmailRegex).Success)
                {
                    sb.AppendLine(R.ErrorEmail);
                }
            }

            if (string.IsNullOrWhiteSpace(Password))
            {
                sb.AppendLine(string.Format(R.EmptyField, R.Password));
            }

            return sb.ToString();
        }

        private DateTime ReadTokedExpiration(string jwt)
        {
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            return token.ValidTo;
        }

        private long ReadTokedUserId(string jwt)
        {
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            if (token.Payload.TryGetValue(Const.UserId, out object result))
            {
                return long.Parse(result.ToString());
            }
            else
            {
                Dialogs.Toast("nepodařilo se nalézt token");
                return 0L;
            }
        }

        public RegisterViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            Title = R.Register;
        }
    }
}
