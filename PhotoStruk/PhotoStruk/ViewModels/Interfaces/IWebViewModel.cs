﻿using PhotoStruk.Services;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoStruk.ViewModels.Interfaces
{
    public interface IWebViewModel
    {
        CancellationTokenSource WebCancellationTokenSource { get; }
        WebService WebService { get; }
        Task LoadDataFromWebAsync();
    }
}
