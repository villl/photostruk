﻿using PhotoStruk.Models.Interfaces;
using PhotoStruk.Services;
using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.ViewModels.Interfaces
{
    public interface IDbViewModel<TEntity> where TEntity : RealmObject, IEntityModel, new()
    {
        DbService<TEntity> DbService { get; }
    }
}
