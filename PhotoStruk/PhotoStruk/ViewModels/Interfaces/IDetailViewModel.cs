﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace PhotoStruk.ViewModels.Interfaces
{
    public interface IDetailViewModel
    {
        ICommand DeleteCommand { get; }
        ICommand SaveCommand { get; }
        ICommand UndoCommand { get; }
        ICommand BackButtonCommand { get; }
        ICommand OptionsCommand { get; }
    }
}
