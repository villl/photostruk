﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.Resources;
using PhotoStruk.Services;
using PhotoStruk.Utils;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PhotoStruk.ViewModels
{
    public class PhotoFormViewModel : BaseViewModel
    {
        public CancellationTokenSource WebCancellationTokenSource => new CancellationTokenSource();
        public WebService WebService => new WebService(Dialogs);

        private MediaFile photoFile;

        private string name;
        public long ObjectId { get; set; }

        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set { SetProperty(ref description, value); }
        }

        private ImageSource photo;

        public ImageSource Photo
        {
            get { return photo; }
            set { SetProperty(ref photo, value); }
        }

        private double? latitude;

        public double? Latitude
        {
            get { return latitude; }
            set { SetProperty(ref latitude, value); }
        }

        private double? longitude;

        public double? Longitude
        {
            get { return longitude; }
            set { SetProperty(ref longitude, value); }
        }

        /// <summary>
        /// Send new photo to the server
        /// </summary>
        public ICommand SendPhotoCommand => new Command(async () =>
        {
            string errorMessage = ValidateForm();
            if (errorMessage.Length > 0)
            {
                await Dialogs.AlertAsync(errorMessage, R.Error, R.Ok);
                return;
            }

            using (this.Dialogs.Loading(R.Loading))
            {

                Photo photo = await WebService.AddPhotoAsync(Name, Description, Latitude, Longitude, ObjectId,
                    photoFile, Preferences.Get(Const.UserId, 0L), WebCancellationTokenSource.Token);
            }
            
            if (photo != null)
            {
                Dialogs.Toast(R.PhotoWasSend);
                await Shell.Current.Navigation.PopAsync();
            }
        });

        /// <summary>
        /// Take a photo
        /// </summary>
        public ICommand TakePhotoCommand => new Command(async () => 
        {
            if (!await PermissionHelper.CheckPermission<Permissions.Camera>())
            {
                await this.Dialogs.AlertAsync(R.PermissionCamera, R.Warning);
                return;
            }

            if (!await PermissionHelper.CheckPermission<Permissions.StorageWrite>())
            {
                await this.Dialogs.AlertAsync(R.PermissionWriteStorage, R.Warning);
                return;
            }

            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsTakePhotoSupported)
            {
                await this.Dialogs.AlertAsync(R.DeviceNotSupport, R.Error);
            }

            StoreCameraMediaOptions options = new StoreCameraMediaOptions()
            {
                PhotoSize = PhotoSize.Medium,
                Directory = "Sample",
                Name = "test.jpg"
            };
            var selectedImage = await CrossMedia.Current.TakePhotoAsync(options);

            if (selectedImage == null)
            {
                await this.Dialogs.AlertAsync(R.ImageNotFound, R.Error);
            }

            photoFile = selectedImage;
            Photo = ImageSource.FromStream(() => selectedImage.GetStream());
        });

        /// <summary>
        /// Select photo from gallery
        /// </summary>
        public ICommand SelectPhotoCommand => new Command(async () => 
        {
            if (!await PermissionHelper.CheckPermission<Permissions.StorageRead>())
            {
                await this.Dialogs.AlertAsync(R.PermissionReadStorage, R.Warning);
                return;
            }

            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await this.Dialogs.AlertAsync(R.DeviceNotSupport, R.Warning);
            }

            PickMediaOptions options = new PickMediaOptions()
            {
                PhotoSize = PhotoSize.Medium
            };
            var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(options);

            if (selectedImageFile == null)
            {
                await this.Dialogs.AlertAsync(R.ImageNotFound, R.Error);
            }

            photoFile = selectedImageFile;
            Photo = ImageSource.FromStream(() => selectedImageFile.GetStream());
        });

        public ICommand LoadLocationCommand => new Command(async () =>
        {
            await LoadLocation();
        });

        private string ValidateForm()
        {
            StringBuilder sb = new StringBuilder();

            if (string.IsNullOrWhiteSpace(Name))
            {
                sb.AppendLine(string.Format(R.EmptyField, R.Name));
            }

            if (photoFile == null || photoFile.GetStream().Length == 0)
            {
                sb.AppendLine(string.Format(R.EmptyField, R.Photo));
            }

            return sb.ToString();
        }

        private async Task LoadLocation()
        {
            Xamarin.Essentials.Location location = await LocationHelper.GetCurrentLocationAsync();

            if (location == null)
            {
                this.Dialogs.Toast(R.LocationNotFound);
                return;
            }

            Latitude = location.Latitude;
            Longitude = location.Longitude;
        }

        public PhotoFormViewModel(long objectId, IUserDialogs dialogs) : base(dialogs)
        {
            Title = R.Photo;
            ObjectId = objectId;
            //LoadLocation();
        }
    }
}
