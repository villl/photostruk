﻿using PhotoStruk.Utils.Enums;
using PhotoStruk.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppShell : Shell
    {
        public ICommand LogoutCommand => new Command(async () =>
        {
            await DisplayAlert("Logout", "You have been logout", "OK");
        });

        public AppShell()
        {
            InitializeComponent();
            RouteRegister();
        }

        private void RouteRegister()
        {
            Routing.RegisterRoute(EPages.Login.ToString(), typeof(LoginPage));
            Routing.RegisterRoute(EPages.Information.ToString(), typeof(InformationPage));
            Routing.RegisterRoute(EPages.Explore.ToString(), typeof(ExplorePage));
            Routing.RegisterRoute(EPages.AbandonedVillages.ToString(), typeof(AbandonedVillagesPage));
            Routing.RegisterRoute(EPages.Settings.ToString(), typeof(SettingsPage));
            Routing.RegisterRoute(EPages.ComponentDetail.ToString(), typeof(ComponentDetailPage)); ;
            Routing.RegisterRoute(EPages.PhotoDetail.ToString(), typeof(PhotoDetailPage));
            Routing.RegisterRoute(EPages.VillageDetail.ToString(), typeof(VillageDetailPage));
            Routing.RegisterRoute(EPages.Search.ToString(), typeof(SearchPage));
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            //await Current.GoToAsync(EPages.Home.ToString()); // Show home page on menu appering
        }
    }
}