﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.CustomElements
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SvgColorableCachedImage : PancakeView, INotifyPropertyChanged
    {
        public SvgColorableCachedImage()
        {
            InitializeComponent();
            //Dictionary<string, string> colorMap = new Dictionary<string, string>
            //    {
            //        { $"#fff", $"{Utils.HexColorHelper.GetHexString(Color.Green)}" }
            //    };
            //IconImage.ReplaceStringMap = colorMap;
        }

        public static readonly BindableProperty CommandParameterProperty =
        BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(ImgButton), null);

        /// <summary>
        /// Parameter send to command
        /// </summary>
        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static readonly BindableProperty CommandProperty =
        BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ImgButton), null);

        /// <summary>
        /// Click command (what happens when user click)
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly BindableProperty IconSourceProperty =
        BindableProperty.Create(nameof(IconSource), typeof(string), typeof(SvgColorableCachedImage), null);

        /// <summary>
        /// Set Icon source string
        /// </summary>
        public string IconSource
        {
            get { return (string)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }

        public static readonly BindableProperty IconColorProperty =
        BindableProperty.Create(nameof(IconColor), typeof(Color), typeof(SvgColorableCachedImage), null);

        public Color IconColor
        {
            get { return (Color)GetValue(IconColorProperty); }
            set { SetValue(IconColorProperty, value); }
        }

        /// <summary>
        /// For pressiong only once
        /// </summary>
        private bool IsPressed = false;

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (IsPressed) return;
            IsPressed = true;
            await Animate();
            Command.Execute(CommandParameter);
            await Task.Delay(30); // Delay for prevent intencional multi click (multiple navigation)
            IsPressed = false;
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(IconSource))
            {
                IconImage.Source = IconSource;
            }
            else if (propertyName == nameof(IconColor) && IconColor != null)
            {
                Dictionary<string, string> colorMap = new Dictionary<string, string>
                {
                    { $"#000000", $"{Utils.HexColorHelper.GetHexString(IconColor)}" }
                };
                IconImage.ReplaceStringMap = colorMap;
            }
            base.OnPropertyChanged(propertyName);
        }

        /// <summary>
        /// Click animation for this button
        /// </summary>
        /// <returns></returns>
        private async Task Animate()
        {
            await this.FadeTo(0.2, 75);
            await this.FadeTo(1, 75);
        }
    }
}