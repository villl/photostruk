﻿using Mapsui.UI.Forms;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.CustomElements
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomMapView : MapView
    {
        public static readonly BindableProperty PinCommandParameterProperty =
        BindableProperty.Create(nameof(PinCommandParameter), typeof(object), typeof(CustomMapView), null);

        /// <summary>
        /// Parameter send to command
        /// </summary>
        public object PinCommandParameter
        {
            get { return GetValue(PinCommandParameterProperty); }
            set { SetValue(PinCommandParameterProperty, value); }
        }

        public static readonly BindableProperty PinCommandProperty =
        BindableProperty.Create(nameof(PinCommand), typeof(ICommand), typeof(CustomMapView), null);

        /// <summary>
        /// Click command (what happens when user click)
        /// </summary>
        public ICommand PinCommand
        {
            get { return (ICommand)GetValue(PinCommandProperty); }
            set { SetValue(PinCommandProperty, value); }
        }

        /// <summary>
        /// For pressiong only once
        /// </summary>
        private bool IsPressed = false;

        public CustomMapView()
        {
        }

        private async void OnPinClicked(object sender, PinClickedEventArgs e)
        {
            if (IsPressed) return;
            IsPressed = true;

            if (e.Pin != null)
            {
                PinCommand.Execute(e.Pin);
            }

            await Task.Delay(30); // Delay for prevent intencional multi click (multiple navigation)
            IsPressed = false;
            e.Handled = true;
        }
    }
}