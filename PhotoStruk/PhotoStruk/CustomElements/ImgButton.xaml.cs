﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace PhotoStruk.CustomElements
{
    public partial class ImgButton : PancakeView, INotifyPropertyChanged
    {
        public static readonly BindableProperty CommandParameterProperty =
        BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(ImgButton), null);

        /// <summary>
        /// Parameter send to command
        /// </summary>
        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static readonly BindableProperty CommandProperty =
        BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ImgButton), null);

        /// <summary>
        /// Click command (what happens when user click)
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly BindableProperty IconColorProperty =
        BindableProperty.Create(nameof(IconColor), typeof(Color), typeof(ImgButton), null);

        public Color IconColor
        {
            get { return (Color)GetValue(IconColorProperty); }
            set { SetValue(IconColorProperty, value); }
        }

        //public static readonly

        /// <summary>
        /// Set Icon source string
        /// </summary>
        public string IconSource
        {
            set { IconImage.Source = value; }
        }

        /// <summary>
        /// For pressiong only once
        /// </summary>
        private bool IsPressed = false;

        public ImgButton()
        {
            InitializeComponent();
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (IsPressed) return;
            IsPressed = true;
            await Animate();
            Command.Execute(CommandParameter);
            await Task.Delay(30); // Delay for prevent intencional multi click (multiple navigation)
            IsPressed = false;
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(IconColor) && IconColor != null)
            {
                Dictionary<string, string> colorMap = new Dictionary<string, string>
                {
                    { $"#000000", $"{Utils.HexColorHelper.GetHexString(IconColor)}" }
                };
                IconImage.ReplaceStringMap = colorMap;
            }
        }

        /// <summary>
        /// Click animation for this button
        /// </summary>
        /// <returns></returns>
        private async Task Animate()
        {
            await this.FadeTo(0.2, 75);
            await this.FadeTo(1, 75);
        }
    }
}