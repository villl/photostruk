﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.CustomElements
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JustifyLabel : Label
    {
        public JustifyLabel()
        {
            InitializeComponent();
        }
    }
}