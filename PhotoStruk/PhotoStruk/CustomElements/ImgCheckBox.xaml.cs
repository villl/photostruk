﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.CustomElements
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImgCheckBox : Grid, INotifyPropertyChanged
    {
        public static readonly BindableProperty IsCheckedProperty =
        BindableProperty.Create(nameof(IsChecked), typeof(bool), typeof(bool), false, BindingMode.TwoWay);

        /// <summary>
        /// Gets or sets a value indicating whether the control is checked.
        /// </summary>
        /// <value>The checked state.</value>
        public bool IsChecked
        {
            get => (bool)this.GetValue(IsCheckedProperty);
            set => SetValue(IsCheckedProperty, value);
        }

        /// <summary>
        /// Label in button
        /// </summary>
        public string Text
        {
            set => TitleLabel.Text = value; 
        }

        /// <summary>
        /// Set Icon source string
        /// </summary>
        public string Source
        {
            set => IconImage.Source = value;
        }

        /// <summary>
        /// For pressing only once
        /// </summary>
        private bool IsPressed = false;

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (IsPressed) return;
            IsPressed = true;
            IsChecked = !IsChecked;
            CheckBox.IsChecked = !CheckBox.IsChecked;
            //if (IsChecked)
            //{
            //    await this.ScaleTo(1.1);
            //}
            //else
            //{
            //    await this.ScaleTo(1);
            //}
            await Task.Delay(30); // Delay for prevent intencional multi click (multiple navigation)
            IsPressed = false;
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(IsChecked) && IsChecked != null)
            {
                IsChecked = IsChecked;
                CheckBox.IsChecked = CheckBox.IsChecked;
            }
        }

        public ImgCheckBox()
        {
            InitializeComponent();
        }
    }
}