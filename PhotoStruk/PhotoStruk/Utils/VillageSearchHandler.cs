﻿using PhotoStruk.Models;
using PhotoStruk.Services;
using PhotoStruk.Views;
using Realms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace PhotoStruk.Utils
{
    public class VillageSearchHandler : SearchHandler
    {
        protected override void OnQueryChanged(string oldValue, string newValue)
        {
            base.OnQueryChanged(oldValue, newValue);

            if (string.IsNullOrWhiteSpace(newValue))
            {
                ItemsSource = null;
            }
            else
            {
                var db = new DbService<Village>();
                ItemsSource = db.GetItemsFilter($"NameCzech CONTAINS '{newValue}'")
                    .ToList();
            }
        }

        protected override async void OnItemSelected(object item)
        {
             base.OnItemSelected(item);

            // Note: strings will be URL encoded for navigation (e.g. "Blue Monkey" becomes "Blue%20Monkey"). Therefore, decode at the receiver.
            await Shell.Current.Navigation.PushAsync(new VillageDetailPage(item as Village));
        }
    }
}
