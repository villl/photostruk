﻿using PhotoStruk.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace PhotoStruk.Utils
{
    public static class MbTilesHelper
    {

        /// <summary>
        /// Copies a number of embedded resources to the local file system.
        /// </summary>
        /// <param name="createFile"></param>
        public static void DeployMbTilesFile(Func<string, Stream> createFile)
        {
            // So what is this all about?
            // I don't know how to access the file as part of the apk (let me know if there is a simple way)
            // So I store them as embedded resources and copy them to disk on startup.
            // (Is there a way to access sqlite files directly as memory stream?).

            var embeddedResourcesPath = "PhotoStruk.Resources.";
            var mbTileFiles = new[] { "world.mbtiles", "kk.mbtiles", "lipno.mbtiles"};

            foreach (var mbTileFile in mbTileFiles)
            {
                CopyEmbeddedResourceToStorage(embeddedResourcesPath, mbTileFile, createFile);
            }
        }

        private static void CopyEmbeddedResourceToStorage(string embeddedResourcesPath, string mbTilesFile,
            Func<string, Stream> createFile)
        {
            var assembly = typeof(BaseViewModel).GetTypeInfo().Assembly;
            using (var image = assembly.GetManifestResourceStream(embeddedResourcesPath + mbTilesFile))
            {
                if (image == null) throw new ArgumentException("EmbeddedResource not found");
                using (var dest = createFile(mbTilesFile))
                {
                    image.CopyTo(dest);
                }
            }
        }
    }
}
