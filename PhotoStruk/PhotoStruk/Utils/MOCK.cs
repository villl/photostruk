﻿using PhotoStruk.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PhotoStruk.Utils
{
    public class MOCK
    {
        //public static List<Village> Villages = new List<Village>
        //{
        //    new Village{Id = 26, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 16.7936, Latitude = 48.7636},
        //    new Village{Id = 27, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 17.9936, Latitude = 48.7656},
        //    new Village{Id = 28, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 15.7936, Latitude = 45.7636},
        //    new Village{Id = 29, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 13.7936, Latitude = 48.7636},
        //    new Village{Id = 30, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 14.0000, Latitude = 48.1636},
        //    new Village{Id = 31, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 14.7936, Latitude = 48.7636},
        //    new Village{Id = 32, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 18.7936, Latitude = 51.7636},
        //    new Village{Id = 29, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 18.7936, Latitude = 58.7636},
        //    new Village{Id = 30, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 16.7936, Latitude = 38.7636},
        //    new Village{Id = 31, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 18.7936, Latitude = 48.7636},
        //    new Village{Id = 32, CzechName = "Veveří", GermanName = "Piberschlag", Areas = "Veveří - centrum", District = "České Budějovice", AdministrativeMunicipality = "Nové Hrady", CatastralArea = "Veveří", Longitude = 14.7936, Latitude = 42.7636}
        //};

        //public static List<Component> Components = new List<Component>
        //{
        //    new Component {Id = 17, CzechName = "č.p. 1", Areas = "Centrum", Longitude = 14.3936, Latitude = 48.7036},
        //    new Component {Id = 12, CzechName = "č.p. 2", Areas = "Centrum", Longitude = 14.1936, Latitude = 48.2656},
        //    new Component {Id = 13, CzechName = "č.p. 3", Areas = "Centrum", Longitude = 14.397, Latitude = 48.6425},
        //    new Component {Id = 14, CzechName = "č.p. 4", Areas = "Centrum", Longitude = 14.5936, Latitude = 47.3636},
        //    new Component {Id = 15, CzechName = "č.p. 5", Areas = "Centrum", Longitude = 14.6936, Latitude = 48.5636},
        //    new Component {Id = 16, CzechName = "č.p. 6", Areas = "Centrum", Longitude = 14.7936, Latitude = 48.7636}
        //};

        public static List<Photograph> Photographs = new List<Photograph>
        {
            new Photograph{Location = "Pohoří na Šumavě", TimePeriod = "1921 - 1930", Longitude = 14.7936, Latitude = 48.1646},
            new Photograph{Location = "Pohoří na Šumavě 1", TimePeriod = "1922 - 1930", Longitude = 14.423, Latitude = 50.0},
            new Photograph{Location = "Pohoří na Šumavě 2", TimePeriod = "1923 - 1930", Longitude = 14.136, Latitude = 48.3777},
            new Photograph{Location = "Pohoří na Šumavě 3", TimePeriod = "1924 - 1930", Longitude = 14.6336, Latitude = 48.1111},
            new Photograph{Location = "Pohoří na Šumavě 4", TimePeriod = "1925 - 1930", Longitude = 15.123, Latitude = 49.0423},
            new Photograph{Location = "Pohoří na Šumavě 5", TimePeriod = "1926 - 1930", Longitude = 15.123, Latitude = 48.8423}
        };

        public static List<Photo> Photos = new List<Photo>
        {
            new Photo{Name = "Fotka 1", Source = ImageSource.FromFile("mockVillage.png")},
            new Photo{Name = "Fotka 2", Source = ImageSource.FromFile("mockVillage2.png")},
            new Photo{Name = "Fotka 3", Source = ImageSource.FromFile("mockVillage3.png")},
            new Photo{Name = "Fotka 4", Source = ImageSource.FromFile("mockVillage4.png")},
            new Photo{Name = "Fotka 5", Source = ImageSource.FromFile("mockVillage5.png")},
        };

        public static List<Information> Informations = new List<Information>
        {
            new Information{Title = "", Detail= ""}
        };
    }
}
