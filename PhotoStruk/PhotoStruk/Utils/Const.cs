﻿namespace PhotoStruk.Utils
{
    public class Const
    {
        public const string DateFormat = "dd/MM/yyyy hh.mm";

        public const string AccessToken = "AccessToken";
        public const string Email = "Email";
        public const string TokenExpiration = "TokenExpiration";
        public const string UserId = "userID"; // not change
        public const string LastSync = "LastSync";

        public const string Search = "Search";
        public const string PhotographsIsToggled = "PhotographsIsToggled";
        public const string VillagesIsToggled = "VillagesIsToggled";
        public const string ComponentsIsToggled = "ComponentsIsToggled";
        public const string LandscapeIsToggled = "LandscapeIsToggled";
        public const string BuildingIsToggled = "BuildingIsToggled";
        public const string GroupIsToggled = "GroupIsToggled";
        public const string SceneIsToggled = "SceneIsToggled";
        public const string PostcardIsToggled = "PostcardIsToggled";
        public const string PortraitIsToggled = "PortraitIsToggled";
        public const string StatueIsToggled = "StatueIsToggled";
        public const string OtherIsToggled = "OtherIsToggled";
        public const string FromYear = "FromYear";
        public const string ToYear = "ToYear";

        // Validation
        public const string EmailRegex = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";

        public const string DbFileName = "PhotoStruk.db3";
    }
}
