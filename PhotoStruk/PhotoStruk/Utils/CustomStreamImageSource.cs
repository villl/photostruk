﻿using Xamarin.Forms;

namespace PhotoStruk.Utils
{
    public class CustomStreamImageSource : StreamImageSource
    {
        public string Key { get; set; }
    }
}
