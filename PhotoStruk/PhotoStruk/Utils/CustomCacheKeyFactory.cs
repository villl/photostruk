﻿using FFImageLoading.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PhotoStruk.Utils
{
    class CustomCacheKeyFactory : ICacheKeyFactory
    {
        public string GetKey(ImageSource imageSource, object bindingContext)
        {
            var keySource = imageSource as CustomStreamImageSource;

            if (keySource == null)
                return null;

            return keySource.Key;
        }
    }
}
