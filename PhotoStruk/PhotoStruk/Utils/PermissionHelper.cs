﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using static Xamarin.Essentials.Permissions;

namespace PhotoStruk.Utils
{
    public static class PermissionHelper
    {
        /// <summary>
        /// Check if permission is granted otherwise ask for permission
        /// </summary>
        /// <typeparam name="TPermission"></typeparam>
        /// <returns></returns>
        public static async Task<bool> CheckPermission<TPermission>() where TPermission : BasePermission, new()
        {
            var hasPermission = await CheckStatusAsync<TPermission>();
            if (hasPermission == PermissionStatus.Granted)
            {
                return true;
            }
            else if(hasPermission == PermissionStatus.Disabled)
            {
                return false;
            }

            var result = await RequestAsync<TPermission>();

            if (result == PermissionStatus.Granted)
            {
                return true;
            }

            return false;
        }
    }
}
