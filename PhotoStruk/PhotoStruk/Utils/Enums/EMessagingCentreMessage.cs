﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Utils.Enums
{
    public enum EMessagingCentreMessage
    {
        DisplayAlert,
        NavigatePage,
        FilterPage,
        NavigatePageBack,
        ShowModal,
        ShowDetail
    }
}
