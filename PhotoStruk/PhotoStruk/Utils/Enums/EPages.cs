﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Utils.Enums
{
    public enum EPages
    {
        About,
        Home,
        Information,
        Explore,
        AbandonedVillages,
        VillageDetail,
        PhotoDetail,
        ComponentDetail,
        Search,
        Login,
        Register,
        Logout,
        Settings,
        Default
    }
}
