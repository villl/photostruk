﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Utils.Enums
{
    public enum ELanguageType
    {
        EN,
        CZ,
        DE
    }
}
