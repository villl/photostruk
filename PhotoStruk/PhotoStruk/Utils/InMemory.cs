﻿using PhotoStruk.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Utils
{
    public class InMemory
    {

        /// <summary>
        /// Key used to authorization on api.
        /// </summary>
        public string ApiKey { get; set; } = string.Empty;

        /// <summary>
        /// Id of signin user.
        /// </summary>
        public long UserId { get; set; } = 0;

        public List<VillageModel> VillageModels = new List<VillageModel>
        {
            new VillageModel {VillageId = 1, Name = "Kapličky", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610002011/Videos/Kapli%C4%8Dky_ftqbjl.mp4"},
            new VillageModel {VillageId = 2, Name = "Boletice", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610002053/Videos/Boletice_v5g179.mp4"},
            new VillageModel {VillageId = 3, Name = "Hůrka", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610002042/Videos/H%C5%AFrka_qhwoul.mp4"},
            new VillageModel {VillageId = 4, Name = "Nová Pec", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610002090/Videos/Nov%C3%A1_Pec_p0atb0.mp4"},
            new VillageModel {VillageId = 5, Name = "Dolní Borková", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610002064/Videos/Doln%C3%AD_Borkov%C3%A1_saygv1.mp4"},
            new VillageModel {VillageId = 6, Name = "Ondřejov", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610002036/Videos/Ond%C5%99ejov_zykj9i.mp4"},
            new VillageModel {VillageId = 7, Name = "Lovecký zámek žofín", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610002018/Videos/loveck%C3%BD_z%C3%A1mek_%C5%BDof%C3%ADn_tefp08.mp4"},
            new VillageModel {VillageId = 8, Name = "Rychnůvek", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610001974/Videos/Rychn%C5%AFvek_N%C4%9Bmeck%C3%BD_Rychnov_sri4gp.mp4"},
            new VillageModel {VillageId = 9, Name = "Dolní Vltavice", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1610001965/Videos/Doln%C3%AD_Vltavice_esqvut.mp4"},
            new VillageModel {VillageId = 10, Name = "Vítěšovice", PublicUrl = "https://res.cloudinary.com/dgtxetndo/video/upload/v1609787305/Videos/V%C3%ADt%C4%9B%C5%A1ovice_f1lgnl.mp4"}
        };
    }
}
