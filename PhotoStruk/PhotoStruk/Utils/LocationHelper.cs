﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace PhotoStruk.Utils
{
    public static class LocationHelper
    {
        /// <summary>
        /// Get current position
        /// </summary>
        /// <returns>Location object</returns>
        public async static Task<Location> GetCurrentLocationAsync()
        {
            try
            {
                Location location = await Geolocation.GetLastKnownLocationAsync();
                if (location == null)
                {
                    location = await Geolocation.GetLocationAsync(new GeolocationRequest
                    {
                        DesiredAccuracy = GeolocationAccuracy.Medium,
                        Timeout = TimeSpan.FromSeconds(30)
                    });
                }

                return location;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
