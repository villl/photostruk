﻿using Acr.UserDialogs;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : BasePage
	{
		public LoginPage ()
		{
			InitializeComponent ();
			BindingContext = new LoginViewModel(UserDialogs.Instance);
		}
	}
}