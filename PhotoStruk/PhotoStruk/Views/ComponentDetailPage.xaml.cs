﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ComponentDetailPage : BaseTabPage
    {
        public ComponentDetailPage()
        {
            InitializeComponent();
        }

        public ComponentDetailPage(object model)
        {
            InitializeComponent();
            BindingContext = new ComponentDetailViewModel(model as Component, UserDialogs.Instance);
        }
    }
}