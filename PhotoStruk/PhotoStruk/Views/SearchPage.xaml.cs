﻿using Acr.UserDialogs;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage : BasePage
    {
        public SearchPage()
        {
            InitializeComponent();
            BindingContext = new SearchViewModel(UserDialogs.Instance);
        }

        private void sliderFrom_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (sliderFrom.Value > sliderTo.Value)
            {
                sliderFrom.Value = sliderTo.Value - 1;
            }
        }

        private void sliderTo_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (sliderTo.Value < sliderFrom.Value)
            {
                sliderTo.Value = sliderFrom.Value + 1;
            }
        }
    }
}