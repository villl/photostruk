﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArealDetailPage : BasePage
    {
        public ArealDetailPage(object model)
        {
            BindingContext = new ArealDetailViewModel(model as Areal, UserDialogs.Instance);
            InitializeComponent();
        }
    }
}