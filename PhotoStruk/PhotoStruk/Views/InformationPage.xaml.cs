﻿using Acr.UserDialogs;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InformationPage : BasePage
	{
		public InformationPage ()
		{
			InitializeComponent ();
			BindingContext = new InformationViewModel(UserDialogs.Instance);
		}
	}
}