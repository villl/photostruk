﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PhotoDetailPage : BaseTabPage
    {
        public PhotoDetailPage()
        {
            InitializeComponent();
        }

        public PhotoDetailPage(object villageImage, object photo)
        {
            BindingContext = new PhotoDetailViewModel(villageImage as VillageImage, photo as Photo, UserDialogs.Instance);
            InitializeComponent();
            MessagingCenter.Subscribe<PhotoDetailViewModel, List<Tag>>(this, "SendTags", (sender, tags) =>
            {
                foreach (var item in tags)
                {
                    PancakeView pancakeView = new PancakeView();
                    var border = new Border();
                    border.Color = Color.White;
                    border.Thickness = 2;
                    pancakeView.Border = border;
                    pancakeView.CornerRadius = new CornerRadius(10);
                    pancakeView.Margin = new Thickness(0, 3);

                    //pancakeView.Style = (Style)Application.Current.Resources["TagView"];
                    Label label = new Label();
                    //label.Style = (Style)Application.Current.Resources["TagLabel"];
                    label.Text = item.Name;
                    label.Padding = new Thickness(5);
                    //label.Margin = new Thickness(0, 3);
                    pancakeView.Content = label;
                    fxLayout.Children.Add(pancakeView);
                }
            });
        }
    }
}