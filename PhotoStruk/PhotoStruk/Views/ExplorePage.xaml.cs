﻿using Acr.UserDialogs;
using BruTile.MbTiles;
using Mapsui;
using Mapsui.Layers;
using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.UI;
using Mapsui.UI.Forms;
using Mapsui.Utilities;
using Mapsui.Widgets.ScaleBar;
using PhotoStruk.Models;
using PhotoStruk.Models.Interfaces;
using PhotoStruk.Services;
using PhotoStruk.Services.Interfaces;
using PhotoStruk.Utils;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Alerts;
using PhotoStruk.Views.Base;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ExplorePage : BasePage
	{
        public Func<MapView, MapClickedEventArgs, bool> Clicker { get; set; }

        public ExplorePage()
        {
            InitializeComponent();
            //InitializeMap();
            this.SlideMenu = new MapAlert();
         }

        protected override void OnAppearing()
        {
            var vm = new ExploreViewModel(UserDialogs.Instance);
            BindingContext = vm;
            vm.MapView = mapView;

            this.SlideMenu.BindingContext = vm;

            mapView.Map = vm.Map;
            vm.ClearPins();
            vm.InitializePins();
            //mapView.IsMyLocationButtonVisible = false;
            //mapView.IsNorthingButtonVisible = false;
            //mapView.IsZoomButtonVisible = false;
            //mapView.RotationLock = true;

            //mapView.MyLocationEnabled = true;
        }

        private async void OnPinClicked(object sender, PinClickedEventArgs e)
        {
            if (e.Pin != null)
            {
                if (!(BindingContext is ExploreViewModel context))
                    return;

                if (e.NumOfTaps == 2)
                {
                    // Hide Pin when double click
                    //DisplayAlert($"Pin {e.Pin.Label}", $"Is at position {e.Pin.Position}", "Ok");
                    e.Pin.IsVisible = false;
                }
                if (e.NumOfTaps == 1)
                {
                    
                    await Xamarin.Essentials.MainThread.InvokeOnMainThreadAsync(() =>
                    {
                        this.ShowMenu();
                        //context.DetailCommand.Execute(e.Pin);
                        context.PinCommand.Execute(e.Pin);
                    });
                    
                    //e.Pin.IsCalloutVisible = !e.Pin.IsCalloutVisible;
                }

            }

            e.Handled = true;
        }
    }
}