﻿using PhotoStruk.ViewModels;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views.Base
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseTabPage : TabbedPage
    {
        public BaseTabPage()
        {
            InitializeComponent();
        }

        public virtual Task Refresh() { return Task.CompletedTask; }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (!(BindingContext is BaseViewModel baseViewModel))
                return;

            if (width > height)
            {
                baseViewModel.IsLandscape = true;
                baseViewModel.IsPortrait = false;
            }
            else
            {
                baseViewModel.IsLandscape = false;
                baseViewModel.IsPortrait = true;
            }
        }
    }
}