﻿using PhotoStruk.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views.Base
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ResponsivePage : BasePage
	{
        public View Landscape { get; set; }
        public View Portrait { get; set; }

        public ResponsivePage ()
		{
			InitializeComponent ();

            Content = Portrait;
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (!(BindingContext is BaseViewModel baseViewModel))
                return;

            if (baseViewModel.IsLandscape)
            {
                Content = Landscape;
            }
            else if (baseViewModel.IsPortrait)
            {
                Content = Portrait;
            }
        }
    }
}