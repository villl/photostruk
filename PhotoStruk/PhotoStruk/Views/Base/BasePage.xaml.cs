﻿using PhotoStruk.ViewModels;
using SlideOverKit;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views.Base
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BasePage : MenuContainerPage
	{
		public BasePage ()
		{
			InitializeComponent ();
		}

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (!(BindingContext is BaseViewModel baseViewModel))
                return;

            if (width > height) // true = landscape, false = portrait
            {
                baseViewModel.IsLandscape = true;
                baseViewModel.IsPortrait = false;
            }
            else
            {
                baseViewModel.IsLandscape = false;
                baseViewModel.IsPortrait = true;
            }
        }
    }
}