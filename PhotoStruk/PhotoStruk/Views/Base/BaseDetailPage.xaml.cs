﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views.Base
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseDetailPage : BasePage
    {
        public BaseDetailPage()
        {
            InitializeComponent();
        }

        public BaseDetailPage(object model)
        {
            InitializeComponent();
        }
    }
}