﻿using Acr.UserDialogs;
using PhotoStruk.Models;
using PhotoStruk.Services;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VillageDetailPage : BaseTabPage
    {
        public long objectId { get; set; }
        public VillageDetailViewModel VM { get; set; }
        public VillageDetailPage()
        {
            Village model = new Village() { NameCzech = "mock village", NameGerman = "arr" };
            BindingContext = new VillageDetailViewModel(model as Village, UserDialogs.Instance);
            InitializeComponent();
            InitialVideoPlayer();
        }

        public VillageDetailPage(object model)
        {
            var village = (Village)model;
            BindingContext = VM = new VillageDetailViewModel(model as Village, UserDialogs.Instance);
            objectId = village.Id;
            InitializeComponent();
            InitialVideoPlayer();
        }

        private void InitialVideoPlayer()
        {
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                videoPlayer.Play();

                videoPlayer.ScaleTo(0.99f);
                videoPlayer.ScaleTo(1.00f);

                return false;
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            VM.LoadPhotosAsync(objectId);
        }
    }
}