﻿using PhotoStruk.Models;
using PhotoStruk.Utils;
using SlideOverKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views.Alerts
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapAlert : SlideMenuView
    {
        public MapAlert()
        {
            InitializeComponent();
            InitializeView();
        }

        private void InitializeView()
        {
            // You must set HeightRequest in this case
            this.HeightRequest = 60;
            // You must set IsFullScreen in this case, 
            // otherwise you need to set WidthRequest, 
            // just like the QuickInnerMenu sample
            this.IsFullScreen = true;
            this.MenuOrientations = MenuOrientation.BottomToTop;

            // You must set BackgroundColor, 
            // and you cannot put another layout with background color cover the whole View
            // otherwise, it cannot be dragged on Android
            this.BackgroundColor = Color.Black;
            this.BackgroundViewColor = Color.Transparent;

            // In some small screen size devices, the menu cannot be full size layout.
            // In this case we need to set different size for Android.
            if (Device.RuntimePlatform == Device.Android)
                this.HeightRequest += 50;
        }
    }
}