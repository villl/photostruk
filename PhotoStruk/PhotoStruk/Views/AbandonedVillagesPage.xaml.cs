﻿using Acr.UserDialogs;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AbandonedVillagesPage : BasePage
	{
		public AbandonedVillagesPage ()
		{
			InitializeComponent ();
			BindingContext = new AbandonedVillagesViewModel(UserDialogs.Instance);
		}
    }
}