﻿using PhotoStruk.Models;
using PhotoStruk.Views.Base;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryDetailPage : BasePage
    {
        public HistoryDetailPage()
        {
            InitializeComponent();
        }

        public HistoryDetailPage(Information history)
        {
            InitializeComponent();
            Title = history.Title;
            lblHistory.Text = history.Detail;
        }
    }
}