﻿using Acr.UserDialogs;
using PhotoStruk.ViewModels;
using PhotoStruk.Views.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : BasePage
	{
        public SettingsViewModel VM { get; set; }
        public SettingsPage ()
		{
			InitializeComponent ();
            BindingContext = VM = new SettingsViewModel(UserDialogs.Instance);
		}

        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!(BindingContext is SettingsViewModel context))
                return;

            context.ChangeLanguage((sender as Picker).SelectedItem.ToString());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            VM.LoadDataFromWebAsync();
        }
    }
}