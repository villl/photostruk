﻿using PhotoStruk.Models.Interfaces;
using Realms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PhotoStruk.Models
{
    public class Photo : IEntityModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Path { get; set; }
        public string PublicId { get; set; }
        public string Url { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public long ImageId { get; set; }
        public long UserId { get; set; }
        [Ignored]
        public User User { get; set; }
        [Ignored]
        public ImageSource Source { get; set; }
        [Ignored]
        public List<Comment> Comments { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
