﻿using PhotoStruk.Utils.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class MenuItem
    {
        public MenuItem(EPages pageType, string title, Type targetType, string iconSource)
        {
            Title = title;
            TargetType = targetType;
            IconSource = iconSource;
            PageType = pageType;
        }

        public string Title { get; set; }
        public string IconSource { get; set; }
        public Type TargetType { get; set; }
        public EPages PageType { get; set; }

        // Factory Method

        public static MenuItem Create(EPages page, string title, Type targetType, string iconSource)
        {
            return new MenuItem(page, title, targetType, iconSource);
        }
    }
}
