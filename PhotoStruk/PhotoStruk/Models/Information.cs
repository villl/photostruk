﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class Information
    {
        public string Title { get; set; }
        public string Detail { get; set; }
        public string ShortDetail => Detail.Length > 50 ? Detail.Substring(0, 50) : Resources.R.HistoryNotFound;

        public Information()
        {

        }

        public Information(string title, string detail)
        {
            this.Title = title;
            this.Detail = detail;
        }
    }
}
