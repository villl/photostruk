﻿using Mapsui.UI.Forms;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models.Interfaces
{
    public interface IPinable
    {
        string LabelId { get; }
        double? Latitude { get; set; }
        double? Longitude { get; set; }
    }
}
