﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models.Interfaces
{
    public interface IListViewModel
    {
        string GetTitle { get; }
        string GetDetail { get; }
    }
}
