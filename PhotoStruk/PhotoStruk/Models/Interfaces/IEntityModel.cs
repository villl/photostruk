﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models.Interfaces
{
    public interface IEntityModel : ICloneable
    {
        long Id { get; set; }
    }
}
