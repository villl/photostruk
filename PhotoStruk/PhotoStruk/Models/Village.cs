﻿using Mapsui.UI.Forms;
using PhotoStruk.Models.Interfaces;
using Realms;
using System;

namespace PhotoStruk.Models
{
    public class Village : RealmObject, IEntityModel, IListViewModel, IPinable
    {
        [PrimaryKey]
        public long Id { get; set; }
        public string NameCzech { get; set; }
        public string NameGerman { get; set; }
        public string District { get; set; }
        public string AdministrativeMunicipality { get; set; }
        public string CadastralArea { get; set; }
        public string Wattercourse { get; set; }
        public float Altitude { get; set; }
        public string LandscapeCharakter { get; set; }
        public string PointsOfIterrest { get; set; }
        public string WattercourseGerman { get; set; }
        public string LandscapeCharakterGerman { get; set; }
        public string PointsOfIterrestGerman { get; set; }
        public string Story { get; set; }
        public string StoryGerman { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public History History { get; set; }

        public string GetTitle => NameCzech;

        public string GetDetail => District;

        public string LabelId => Id.ToString();

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
