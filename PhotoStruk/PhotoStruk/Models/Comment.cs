﻿using PhotoStruk.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class Comment
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public User User { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
    }
}
