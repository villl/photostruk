﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class VillageModel
    {
        public long VillageId { get; set; }
        public string Name { get; set; }
        public string PublicUrl { get; set; }
    }
}
