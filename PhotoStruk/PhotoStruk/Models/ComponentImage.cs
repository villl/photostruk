﻿using PhotoStruk.Models.Interfaces;
using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class ComponentImage : RealmObject, IEntityModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public int ComponentId { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
