﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class LoginResponse
    {
        public string Token { get; set; }
    }
}
