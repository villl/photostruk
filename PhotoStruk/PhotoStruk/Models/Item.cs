﻿using PhotoStruk.Models.Interfaces;
using System;

namespace PhotoStruk.Models
{
    public class Item : IListViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }

        public string GetTitle => Title;

        public string GetDetail => Detail;
    }
}