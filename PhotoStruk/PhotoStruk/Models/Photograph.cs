﻿using Mapsui.UI.Forms;
using PhotoStruk.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class Photograph : IPinable
    {
        public string Location { get; set; }
        public string TimePeriod { get; set; }
        public string Category { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public string LabelId => Location;

    }
}
