﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class Destruction : RealmObject
    {
        public string Degree { get; set; }
        public string Type { get; set; }
        public string Demolition { get; set; }
        public string MaterialRecycling { get; set; }
        public string DisposalOfResidues { get; set; }
        public string SurfaceAdjustment { get; set; }
        public string TerrainRelicts { get; set; }
    }
}
