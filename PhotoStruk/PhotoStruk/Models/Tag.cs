﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoStruk.Models
{
    public class Tag
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
