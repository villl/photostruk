﻿using PhotoStruk.Models.Interfaces;
using Realms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PhotoStruk.Models
{
    public class VillageImage : RealmObject, IEntityModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public int VillageId { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        [Ignored]
        public ImageSource Source { get; set; }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
