﻿using Realms;

namespace PhotoStruk.Models
{
    public class History : RealmObject
    {
        [PrimaryKey]
        public long Id { get; set; }
        public string MiddleAges { get; set; }
        public string EarlyModernAges { get; set; }
        public string NineteenthCentury { get; set; }
        public string StateBefore1945 { get; set; }
        public string StateAfter1945 { get; set; }
        public string State1989 { get; set; }
        public string Present { get; set; }
        public string MiddleAgesGerman { get; set; }
        public string EarlyModernAgesGerman { get; set; }
        public string NineteenthCenturyGerman { get; set; }
        public string StateBefore1945German { get; set; }
        public string StateAfter1945German { get; set; }
        public string State1989German { get; set; }
        public string PresentGerman { get; set; }
    }
}
