﻿using PhotoStruk.Models.Interfaces;
using Realms;
using System;

namespace PhotoStruk.Models
{
    public class Areal : RealmObject, IEntityModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public int? VillageId { get; set; }
        public string Description { get; set; }
        public string DescriptionGerman { get; set; }
        public string Degree { get; set; }
        public string Type { get; set; }
        public string MaterialRecycling { get; set; }
        public string DisposalOfResidues { get; set; }
        public string SurfaceAdjusment { get; set; }
        public string TerrainRelicts { get; set; }
        public string DegreeGerman { get; set; }
        public string TypeGerman { get; set; }
        public string MaterialRecyclingGerman { get; set; }
        public string DisposalOfResiduesGerman { get; set; }
        public string SurfaceAdjusmentGerman { get; set; }
        public string TerrainRelictsGerman { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
