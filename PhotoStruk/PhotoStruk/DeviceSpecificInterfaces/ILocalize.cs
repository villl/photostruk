﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace PhotoStruk.DeviceSpecificInterfaces
{
    public interface ILocalize
    {
        CultureInfo GetCultureInfo();
        void SetLocate(CultureInfo ci);
    }
}
