﻿using PhotoStruk.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PhotoStruk.DeviceSpecificInterfaces
{
    public interface IDeviceFile
    {
        string GetFilePath(string fileName);
        Task<string> GetFileAsync(string fileName);
        Task<List<Village>> GetVillages();
        Task<List<Component>> GetComponents();
        Task<List<Photograph>> GetPhotographs();
    }
}
