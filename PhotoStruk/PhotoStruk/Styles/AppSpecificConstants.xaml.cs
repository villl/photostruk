﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Styles
{
    public static class ApplicationConst
    {
        public static int AndroidBorderCoef = 3;
    }

    public static class ApplicationColors
    {
        public static Color PrimaryColor => Color.FromHex("#FFFFFF"); // pri zmene - zmenit v .xaml
        public static Color SecondaryColor => Color.FromHex("#222222"); // pri zmene - zmenit v .xaml
        public static Color DetailColor => Color.FromHex("#696969"); // pri zmene - zmenit v .xaml
        public static Color DisabledColor => Color.FromHex("#696969"); // pri zmene - zmenit v .xaml
        public static Color LabelColor => Color.FromHex("#696969"); // pri zmene - zmenit v .xaml
        public static Color ButtonColor => Color.FromHex("#D9456F"); // pri zmene - zmenit v .xaml
        public static Color BlackColor => Color.FromHex("#000000"); // pri zmene - zmenit v .xaml
        public static Color WhiteColor => Color.FromHex("#FFFFFF"); // pri zmene - zmenit v .xaml
        public static Color BackgroundColor => Color.FromHex("#676767"); // pri zmene - zmenit v .xaml
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AppSpecificConstants : ResourceDictionary
	{
		public AppSpecificConstants ()
		{
			InitializeComponent ();
		}
	}
}