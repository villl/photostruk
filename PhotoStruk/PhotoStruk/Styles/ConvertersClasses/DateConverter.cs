﻿using PhotoStruk.Utils;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace PhotoStruk.Styles.ConvertersClasses
{
    public class DateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() == typeof(DateTime))
            {
                return ((DateTime)value).ToString(Const.DateFormat);
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
