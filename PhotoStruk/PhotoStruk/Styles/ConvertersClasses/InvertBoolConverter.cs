﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace PhotoStruk.Styles.ConvertersClasses
{
    public class InvertBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() == typeof(bool))
            {
                return !(bool)value;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() == typeof(bool))
            {
                return !(bool)value;
            }
            return value;
        }
    }
}
