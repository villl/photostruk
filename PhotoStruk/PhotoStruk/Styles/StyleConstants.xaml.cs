﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PhotoStruk.Styles
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StyleConstants : ResourceDictionary
	{
		public StyleConstants ()
		{
			InitializeComponent ();
		}
	}
}